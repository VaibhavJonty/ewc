

import 'package:EWC/managers/auth_manager.dart';

import 'apis/interceptor.dart';
import 'packages.dart';

final Injector = GetIt.instance;

void registerDependencies() {
  _registerApis();
  _registerManagers();
}

void _registerApis() {
  Injector.registerLazySingleton<Dio>(() {
    var options = BaseOptions(
      baseUrl: ApiConstants.baseUrl,
    );
    Dio dio = new Dio(options);
    dio.interceptors.add(AuthInterceptor());
    dio.interceptors.add(LogInterceptor(requestBody: true, responseBody: true));
    return dio;
  });
  Injector.registerLazySingleton<WebApi>(() {
    return BaseWebApi(Injector.get());
  });
}

void _registerManagers() {
  Injector.registerSingleton<PropertyManager>(
    BasePropertyManager(Injector.get()),
  );
  Injector.registerSingleton<AuthManager>(
    BaseAuthManager(Injector.get()),
  );

}
