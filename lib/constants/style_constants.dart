import 'package:flutter/material.dart';

import 'constants.dart';

abstract class StyleConstants {
  static const formInputFieldBorder = OutlineInputBorder(
    borderSide: BorderSide(color: ColorConstants.formFieldBorder),
  );
  static final formTextViewFieldBorder = Border.all(
    color: ColorConstants.formFieldBorder,
  );
  static final formTextViewFieldBorderRad = BorderRadius.circular(4);
}
