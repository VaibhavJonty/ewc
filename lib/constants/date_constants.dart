import 'package:intl/intl.dart';

abstract class DateConstants {
  static final DateFormat serverDateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
  static final DateFormat monthYearFormat = DateFormat("MMM/yyyy");
  static final DateFormat monthFullYearFormat = DateFormat("MMMM/yyyy");
  static final DateFormat monthDayYearFormat = DateFormat("MM/dd/yyyy");
  static final DateFormat dayMonthYearFormat = DateFormat("dd/MM/yyyy");
  static final DateFormat yearMonthDayFormat = DateFormat("yyyy-MM-dd");
  static final DateFormat YearFormat = DateFormat("yyyy");
}
