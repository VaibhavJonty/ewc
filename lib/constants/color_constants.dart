import 'package:flutter/material.dart';

abstract class ColorConstants {
  static const primaryDarkColor = Color.fromARGB(255, 0,0,0);
  static const primaryColor = Color.fromARGB(255, 255, 255, 255);
  static const whiteDark = Color.fromARGB(255, 255, 255, 255);
  static const accentColor = Color.fromARGB(255, 0,0,0);
  static const transparentColor = Color.fromARGB(255, 163,136,136);
  static const tabSelectedColor = primaryColor;
  static const tabUnSelectedColor = Color.fromARGB(255, 138, 138, 143);

  static const greyDark = Color.fromARGB(255, 97, 97, 97);
  static const greyLight = Color.fromARGB(255, 158, 158, 158);
  static const greyDisabled = Color.fromARGB(255, 238, 238, 238);

  static const blackDark = Color.fromARGB(255, 0, 0, 0);
  static const blackLight = Color.fromARGB(255, 100, 100, 100);

  static const greenDark = Color.fromARGB(255, 67, 160, 71);
  static const greenLight = Color.fromARGB(255, 165, 214, 167);
  
  static const redDark = Color.fromARGB(255, 244, 67, 54);
  static const redLight = Color.fromARGB(255, 229, 115, 115);

  static const buttonNegative = Color.fromARGB(255, 240, 157, 114);

  static const viewColor = Color(0xffa8a8a8);
  static const formFieldBorder = blackLight;
  static const formFieldLabelTextColor = blackLight;

  static const pageHeaderBackColor = greyDisabled;
}
