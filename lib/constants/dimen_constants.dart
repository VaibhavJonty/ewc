import 'package:flutter/material.dart';

abstract class DimenConstants {
  static const windowPad = 16.0;
  static const windowPadSmall = 8.0;
  static const windowPadMini = 4.0;

  static const dialogCornerRadius = 8.0;

  static const headerPaddingAll = EdgeInsets.all(8.0);

  static const verticalSpace4 = SizedBox(height: 4);
  static const verticalSpace8 = SizedBox(height: 8);
  static const verticalSpace16 = SizedBox(height: 16);

  static const formPadding = EdgeInsets.fromLTRB(24, 12, 24, 12);
  static const formFieldPaddingSmall = EdgeInsets.all(12);
  static const textFieldLRPadding8 = EdgeInsets.only(left: 8, right: 8);

  static const listItemCornerRad = 4.0;

  static const pageHeaderHeight = 28.0;

  static const buttonHeightSmall = 28.0;
}
