

abstract class ApiConstants {

  static const baseUrl = "https://tweaksoftwares.com/real-estate-app/api/Authentication/";
  //static const baseAuthUrl = "http://3.15.216.243/api";
  static const baseImageUrl = "https://www.thennenterprise.com/wp-json";


  // User
  static const login = "/login";
  static const home = "/home";
  static const register = "/register";
  static const forgotPassword = "/forgotOtp";
  static const changePassword = "/changePassword";
  static const propertyListing = "/getPropertyList";
  static const propertyLike = "/addToFav";
  static const propertyDetails = "/getPropertyDes";
  static const favPropertyListing = "/getFavList";
  static const searchBarListing = "/getSearchBarList";
  static const editProfile = "/profile";
  static const pageLimit = 10;
}
