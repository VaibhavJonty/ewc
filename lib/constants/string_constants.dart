abstract class StringConstants {
  static const appName = "Ankit weds Sneha";

  static const retryButton = "Retry";
  static const refreshButton = "Refresh";
  static const okButton = "OK";
  static const cancelButton = "Cancel";
  static const detail = "Detail";

  static const errUnknownErrorOccurred = "Unknown error occurred.";

  static const popupTitleSuccess = 'Success';
  static const popupTitleError = 'Error';
  static const popupTitleConfirm = 'Confirm';

  static const comingSoon = 'Coming Soon';
  static const complain = 'Complain';
  static const fitting = 'Fitting';
  static const contact = 'Contact';
  static const history = 'History';
  static const notification = 'Notification';

  static const mobile = "Mobile";
  static const password = "Password";
  static const login = "Login";
  static const familyDetails = "Family Details";
  static const groom = "Groom";
  static const bride = "Bride";
  static const family = "Family";
  static const blessing = "Blessing";
  static const ourStory = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
  +"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et";
}
