import 'dart:convert';

import 'package:EWC/controller/fav_properties_helper.dart';
import 'package:EWC/fragment/property_details.dart';
import 'package:EWC/models/messages.dart';
import 'package:EWC/models/property/PropertyListResponse.dart';
import 'package:EWC/models/propertyitem.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

import '../packages.dart';

class FavouritePropertyPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return FavouritePropertyPageState();
  }
}

class FavouritePropertyPageState extends State<FavouritePropertyPage> {
  PropertyManager propertyManager = Injector.get();
  RxCommandListener<String, PropertyListResponse>
  propertyListListener;
  RxCommandListener<PropertyLikeParameter, PropertyLikeResponse>
  propertyLikeListener;
  bool _loading = false;
  PropertyListResponse propertyListResponse;
  List<Property> propertyList;
  String userId;
  int likePosition = 0;
  void _setLoading(bool value) {
    if (!mounted) return;
    setState(() => _loading = value);
  }

  Future<void> getPropertList() async {
    PrefManager manager = await PrefManager.getInstance();
    String user_id = await manager.getUserId();
    setState(() {
      userId = user_id;
    });
    propertyManager.likedPropertyListing.execute(userId);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    propertyList = [];
    getPropertList();
    propertyListListener = RxCommandListener(
      propertyManager.likedPropertyListing,
      onIsBusy: () => _setLoading(true),
      onNotBusy: () => _setLoading(false),
      onError: (error) {
        print('${error.toString()}');
      },
      onValue: (response) {
        if (!mounted) return;
        setState(() {
          propertyListResponse = response;
          if (propertyListResponse.status == 200) {
            setState(() {
              propertyList.addAll(propertyListResponse.data);
            });
            DialogHelper.showSuccessToast(propertyListResponse.message);
          } else {
            DialogHelper.showErrorToast(propertyListResponse.message);
          }
        });
      },
    );

    propertyLikeListener = RxCommandListener(
      propertyManager.propertyLikeDislike,
      onIsBusy: () => _setLoading(true),
      onNotBusy: () => _setLoading(false),
      onError: (error) {
        print('${error.toString()}');
      },
      onValue: (response) {
        if (!mounted) return;
        setState(() {
          PropertyLikeResponse propertyLikeResponse = response;
          if (propertyLikeResponse.status == 200) {
            setState(() {
              propertyList[likePosition] = propertyLikeResponse.data;
            });
            DialogHelper.showSuccessToast(propertyLikeResponse.message);
          } else {
            DialogHelper.showErrorToast(propertyLikeResponse.message);
          }
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Favourite Properties",
          style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontFamily: 'OpenSans',
              fontWeight: FontWeight.w600),
        ),
      ),
      body:  ModalProgressHUD(
        inAsyncCall: _loading,
        progressIndicator: Loader(),
        child: propertyList.isNotEmpty ?
         _listProperty(propertyList) : Container(),
      )
    ,
    );
  }



  ListView _listProperty(data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) =>
          buildPropertyCard(context, data[index], index),
    );
  }

  buildPropertyCard(BuildContext context, Property property, int position) {
    return Card(
      margin: EdgeInsets.all(6),
      elevation: 3.0,
      color: Colors.indigo[50],
      child: Padding(
        padding: EdgeInsets.all(0.0),

        //padding: EdgeInsets.only(left: 10.0, right: 10.0),
        child: Material(
          //elevation: 1.0,
          color: Colors.transparent,

          borderRadius: BorderRadius.circular(10.0),
          child: new GestureDetector(
            onTap: () {
              //print("Container clicked");

              Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (BuildContext context) => new PropertyDetailsPage(
                      property),
                ),
              );

              // Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //       builder: (context) => PropertyDetailsPage()),
              // );
            },
            child: Container(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Material(
                          elevation: 0.2,
                          // borderRadius: BorderRadius.all(Radius.elliptical(50.0, 50.0)),
                          child: Container(
                            width: 100.0,
                            height: 100.0,
                            child: CachedNetworkImage(
                              imageUrl:
                              property.image,
                              height: 200,
                              fit: BoxFit.cover,
                              placeholder: (context, value) =>
                                  Container(
                                    child: CupertinoActivityIndicator(),
                                  ),
                              errorWidget: (context, value, error) =>
                                  Container(
                                    child: Icon(Icons.broken_image),
                                  ),
                            ),
                            // child: Image.network(
                            //   property.propertiesimage,
                            //   fit: BoxFit.cover,
                            // ),
                            // child: Image(
                            //   fit: BoxFit.cover,
                            //   image: AssetImage('images/house.jpg'),
                            // ),
                          ),
                        ),
                      ),
                      Container(
                        //padding: new EdgeInsets.only(left: 8.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            new Text(
                              "\$" + property.price + " AUD",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 16.0,
                                color: Colors.blue[900],
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5,
                                fontFamily: "OpenSans",
                              ),
                            ),
                            new Text(
                              property.location,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5,
                                fontFamily: "OpenSans",
                              ),
                            ),
                            SizedBox(height: 5),
                            new Text(
                              property.name,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5,
                                fontFamily: "OpenSans",
                              ),
                            ),
                            new Text(
                              property.coveredArea,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5,
                                fontFamily: "OpenSans",
                              ),
                            ),
                            new Text(
                              property.ownerType,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5,
                                fontFamily: "OpenSans",
                              ),
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Container(
                        //color: Colors.pink,
                        height: 120.0,
                        width: 50,
                        child: Stack(
                          children: [
                            Positioned(
                              // top: 5,
                              right: 5,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  SizedBox(
                                    height: 27,
                                    width: 30,
                                    child: IconButton(
                                      iconSize: 15,
                                      onPressed: () {
                                        setState(() {
                                          likePosition = position;
                                          var req = PropertyLikeParameter(
                                              id: userId,
                                              propertyId: property.id,
                                              type: !(property.like)
                                          );
                                          propertyManager.propertyLikeDislike.execute(req);
                                        });
                                      },
                                      icon: !property.like
                                          ? Icon(Icons.favorite_border,
                                          color: Colors.blue[900], size: 20)
                                          : Icon(Icons.favorite,
                                          color: Colors.red[300], size: 20),
                                    ),
                                  ),

                                  SizedBox(width: 5),
                                  CupertinoButton(
                                    minSize: double.minPositive,
                                    padding: EdgeInsets.zero,
                                    child: Image.asset(
                                      "images/shield.png",
                                      color: Colors.greenAccent[400],
                                      height: 18,
                                      width: 16,
                                    ),
                                    onPressed: () {},
                                  ),
                                  // SizedBox(
                                  //   height: 30,
                                  //   //width: 30,
                                  //   child:
                                  // ),
                                ],
                              ),
                            ),
                            Positioned(
                              bottom: -13,
                              right: 0,
                              child: MaterialButton(
                                onPressed: () {},
                                color: Colors.blue[900],
                                minWidth: 50,
                                height: 20,
                                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),

                                //padding: EdgeInsets.all(0),
                                child: Text(
                                  'Buy',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 10,
                                      fontFamily: 'OpenSans',
                                      fontWeight: FontWeight.w600),
                                ),
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topLeft: Radius.circular(8.0),
                                      bottomRight: Radius.circular(8.0)),
                                  side: BorderSide(
                                      color: Colors.transparent,
                                      width: 1,
                                      style: BorderStyle.solid),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

// ignore: missing_return
  Future<Messages> addToWishList(String propertiesid, String favStatus) async {
    final http.Response response = await http.post(
      'https://fituniverse.com/EWC/api/FavResponse',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'properties_id': propertiesid,
        'status_fav': favStatus,
      }),
    );
    if (response.statusCode == 200) {
      //pr.hide();
      var statusMsg = Messages.fromJson(jsonDecode(response.body));
      //SharedPreferences prefs = await SharedPreferences.getInstance();
      if (statusMsg.status == 1) {
        print(statusMsg.status);
        print(statusMsg.message);
// ignore: invalid_use_of_protected_member
        (context as Element).reassemble();
        // Fluttertoast.showToast(
        //     msg: statusMsg.message,
        //     toastLength: Toast.LENGTH_SHORT,
        //     gravity: ToastGravity.CENTER,
        //     timeInSecForIosWeb: 1,
        //     backgroundColor: Colors.black,
        //     textColor: Colors.white,
        //     fontSize: 12.0);

      } else {
        print(statusMsg.message);
        Fluttertoast.showToast(
            msg: statusMsg.message,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.black,
            textColor: Colors.white,
            fontSize: 12.0);
      }
      print(statusMsg.status);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to load Login');
    }
  }
}

// Widget blogList(context) {
//   return Padding(
//     padding: EdgeInsets.all(8.0),

//     //padding: EdgeInsets.only(left: 10.0, right: 10.0),
//     child: Material(
//       color: Colors.indigo[50],
//       elevation: 1.0,
//       borderRadius: BorderRadius.circular(15.0),
//       child: new GestureDetector(
//         onTap: () {
//           //print("Container clicked");
//            String propertyId = "propertiesid";
//           Navigator.push(
//               context,
//               new MaterialPageRoute(
//                   builder: (BuildContext context) =>
//                       new PropertyDetailsPage(propertyId)));
//         },
//         child: Container(
//           child: Column(
//             children: <Widget>[
//               Row(
//                 children: <Widget>[
//                   Padding(
//                     padding: EdgeInsets.all(10),
//                     child: Material(
//                       elevation: 0.2,
//                       // borderRadius: BorderRadius.all(Radius.elliptical(50.0, 50.0)),
//                       child: Container(
//                         width: 100.0,
//                         height: 100.0,
//                         child: Image(
//                           fit: BoxFit.cover,
//                           image: AssetImage('images/house.jpg'),
//                         ),
//                       ),
//                     ),
//                   ),
//                   Container(
//                     //padding: new EdgeInsets.only(left: 8.0),
//                     child: new Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       mainAxisAlignment: MainAxisAlignment.spaceAround,
//                       children: <Widget>[
//                         new Text(
//                           "\$ 50,000 AUD",
//                           textAlign: TextAlign.left,
//                           style: TextStyle(
//                             fontSize: 16.0,
//                             color: Colors.blue[900],
//                             fontWeight: FontWeight.w600,
//                             letterSpacing: 0.5,
//                             fontFamily: "OpenSans",
//                           ),
//                         ),
//                         new Text(
//                           'Sec 4, Salt Lake City, Kolkata',
//                           style: TextStyle(
//                             fontSize: 8.0,
//                             color: Colors.black,
//                             fontWeight: FontWeight.w600,
//                             letterSpacing: 0.5,
//                             fontFamily: "OpenSans",
//                           ),
//                         ),
//                         SizedBox(height: 5),
//                         new Text(
//                           '4 BHK/Flat, House/Villa',
//                           style: TextStyle(
//                             fontSize: 8.0,
//                             color: Colors.black,
//                             fontWeight: FontWeight.w600,
//                             letterSpacing: 0.5,
//                             fontFamily: "OpenSans",
//                           ),
//                         ),
//                         new Text(
//                           '436 Sqft',
//                           style: TextStyle(
//                             fontSize: 8.0,
//                             color: Colors.black,
//                             fontWeight: FontWeight.w600,
//                             letterSpacing: 0.5,
//                             fontFamily: "OpenSans",
//                           ),
//                         ),
//                         new Text(
//                           'Ownership',
//                           style: TextStyle(
//                             fontSize: 8.0,
//                             color: Colors.black,
//                             fontWeight: FontWeight.w600,
//                             letterSpacing: 0.5,
//                             fontFamily: "OpenSans",
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                   Spacer(),
//                   Container(
//                     //color: Colors.pink,
//                     height: 120.0,
//                     width: 50,
//                     child: Stack(
//                       children: [
//                         Positioned(
//                           top: 5,
//                           right: 5,
//                           child: Row(
//                             crossAxisAlignment: CrossAxisAlignment.end,
//                             children: [
//                               CupertinoButton(
//                                 minSize: double.minPositive,
//                                 padding: EdgeInsets.zero,
//                                 child: Icon(Icons.favorite,
//                                     color: Colors.red[300], size: 20),
//                                 onPressed: () {},
//                               ),
//                               SizedBox(width: 5),
//                               CupertinoButton(
//                                 minSize: double.minPositive,
//                                 padding: EdgeInsets.zero,
//                                 child: Image.asset(
//                                   "images/shield.png",
//                                   color: Colors.greenAccent[400],
//                                   height: 18,
//                                   width: 16,
//                                 ),
//                                 onPressed: () {},
//                               ),
//                             ],
//                           ),
//                         ),
//                         Positioned(
//                           //top: 0,
//                           bottom: -13,
//                           right: 0,

//                           child: MaterialButton(
//                             onPressed: () {},
//                             color: Colors.blue[900],
//                             minWidth: 50,
//                             height: 20,
//                             padding: EdgeInsets.fromLTRB(5, 5, 5, 5),

//                             //padding: EdgeInsets.all(0),
//                             child: Text(
//                               'Hold',
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontSize: 10,
//                                   fontFamily: 'OpenSans',
//                                   fontWeight: FontWeight.w600),
//                             ),
//                             shape: new RoundedRectangleBorder(
//                               borderRadius: new BorderRadius.only(
//                                   topLeft: Radius.circular(8.0),
//                                   bottomRight: Radius.circular(8.0)),
//                               side: BorderSide(
//                                   color: Colors.transparent,
//                                   width: 1,
//                                   style: BorderStyle.solid),
//                             ),
//                           ),

//                           // Container(
//                           //   padding: EdgeInsets.fromLTRB(10, 5, 10,5),
//                           //   decoration: BoxDecoration(
//                           //     color: Colors.blue[900],
//                           //     shape: BoxShape.rectangle,
//                           //     borderRadius: BorderRadius.only(
//                           //       topLeft: Radius.circular(8.0),
//                           //       bottomRight: Radius.circular(8.0),
//                           //     ),
//                           //   ),
//                           //   child: Text(
//                           //     "Hold",
//                           //     style: TextStyle(
//                           //         color: Colors.white,
//                           //         fontSize: 10,
//                           //         fontFamily: 'OpenSans',
//                           //         fontWeight: FontWeight.w600),
//                           //   ),
//                           // ),
//                         ),
//                       ],
//                     ),
//                   ),
//                   // Stack(
//                   //   children: <Widget>[
//                   //     // Container(
//                   //     //   color: Colors.blue,
//                   //     //   height: 100.0,
//                   //     // ),
//                   //     Container(
//                   //       //color: Colors.pink,
//                   //       height: 120.0,
//                   //       width: 60,
//                   //       child: Column(
//                   //         children: [
//                   //           Padding(
//                   //             padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
//                   //             child: Align(
//                   //               alignment: Alignment.topRight,
//                   //               child: Row(
//                   //                 crossAxisAlignment: CrossAxisAlignment.end,
//                   //                 children: [
//                   //                   CupertinoButton(
//                   //                     minSize: double.minPositive,
//                   //                     padding: EdgeInsets.zero,
//                   //                     child: Icon(Icons.favorite,
//                   //                         color: Colors.red[400], size: 20),
//                   //                     onPressed: () {},
//                   //                   ),
//                   //                   SizedBox(width: 8),
//                   //                   CupertinoButton(
//                   //                     minSize: double.minPositive,
//                   //                     padding: EdgeInsets.zero,
//                   //                     child: Image.asset(
//                   //                       "images/shield.png",
//                   //                       color: Colors.greenAccent[400],
//                   //                       height: 18,
//                   //                       width: 16,
//                   //                     ),
//                   //                     onPressed: () {},
//                   //                   ),
//                   //                 ],
//                   //               ),
//                   //             ),
//                   //           ),
//                   //           Padding(
//                   //             padding: EdgeInsets.fromLTRB(0, 45, 5, 0),
//                   //             child: Align(
//                   //               alignment: Alignment.topRight,
//                   //               child: MaterialButton(
//                   //                 onPressed: () {},
//                   //                 color: Colors.blue[900],
//                   //                 minWidth: 40,
//                   //                 height: 20,
//                   //                 padding: EdgeInsets.fromLTRB(5, 0, 5, 0),

//                   //                 //padding: EdgeInsets.all(0),
//                   //                 child: Text(
//                   //                   'Hold',
//                   //                   style: TextStyle(
//                   //                       color: Colors.white,
//                   //                       fontSize: 10,
//                   //                       fontFamily: 'OpenSans',
//                   //                       fontWeight: FontWeight.w600),
//                   //                 ),
//                   //                 shape: new RoundedRectangleBorder(
//                   //                   borderRadius:
//                   //                       new BorderRadius.circular(6.0),
//                   //                   side: BorderSide(
//                   //                       color: Colors.transparent,
//                   //                       width: 1,
//                   //                       style: BorderStyle.solid),
//                   //                 ),
//                   //               ),
//                   //             ),
//                   //           ),
//                   //         ],
//                   //       ),
//                   //     ),
//                   //   ],
//                   // ),
//                 ],
//               ),
//             ],
//           ),
//         ),
//       ),
//     ),
//   );
// }
