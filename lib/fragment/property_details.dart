import 'dart:convert';

import 'package:EWC/fragment/MyApp.dart';
import 'package:EWC/models/index.dart';
import 'package:EWC/models/messages.dart';
import 'package:EWC/models/property/PropertyListResponse.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
//import 'package:flutter_braintree/flutter_braintree.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import '../packages.dart';

class PropertyDetailsPage extends StatefulWidget {
  //PropertyDetailsPage(String propertyId);
  final Property property;

  PropertyDetailsPage(
      this.property,
      );
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PropertyDetailsPageState(
        property,
       );
  }
}

class PropertyDetailsPageState extends State<PropertyDetailsPage> {
  PropertyManager propertyManager = Injector.get();
  RxCommandListener<PropertyParameter, PropertyListResponse>
  propertyListListener;
  RxCommandListener<PropertyLikeParameter, PropertyLikeResponse>
  propertyLikeListener;
  bool _loading = false;
  String userId;
  Property property;
  String clientNonce =
      "MIIBCgKCAQEAsmALTH9JmGVh9hBKTiKbtYSXHDZT2/KVc2odeDeeTv76N1ePQe/S+uyICT9awp6LR/cvotQlCFtlSIz+cV+YnMB0a5gJZsh+sZD3gTDpiY30x3NLRytzeBEKlknEA4WDJB5XgJUrt7REi1VFStoGFJEk/xmIcFTsRpxUrLv6ghNf3FUG3Sfovv0TwonAUshiiK4BL9UhpJGk09UZLswplZX51e1pSM6yk0E11Mr+cMiYFx45pLjQLKxBfxn0H3TqhRdSd8pSPeTQnDbLTAF/eKCdjEpdGvHiCVKa76LZrLWVuZd7u9jUpZCtyuK8Vz/M6NlgKIUpsmGFZOTkLagzywIDAQAB";
  static final String tokenizationKey = 'sandbox_6m5hcwqj_5msr7hzpmqgdzhkx';

  // void showNonce(BraintreePaymentMethodNonce nonce) {
  //   showDialog(
  //     context: context,
  //     builder: (_) => AlertDialog(
  //       title: Text('Payment method nonce:'),
  //       content: Column(
  //         mainAxisSize: MainAxisSize.min,
  //         crossAxisAlignment: CrossAxisAlignment.stretch,
  //         children: <Widget>[
  //           Text('Nonce: ${nonce.nonce}'),
  //           SizedBox(height: 16),
  //           Text('Type label: ${nonce.typeLabel}'),
  //           SizedBox(height: 16),
  //           Text('Description: ${nonce.description}'),
  //         ],
  //       ),
  //     ),
  //   );
  // }

  // Future<void> initBrainTreePayment(String amount) async {
  //
  //     var request = BraintreeDropInRequest(
  //       tokenizationKey: tokenizationKey,
  //       collectDeviceData: true,
  //       googlePaymentRequest: BraintreeGooglePaymentRequest(
  //         totalPrice: amount,
  //         currencyCode: 'USD',
  //         billingAddressRequired: false,
  //       ),
  //       paypalRequest: BraintreePayPalRequest(
  //         amount: amount,
  //         displayName: 'EWC',
  //       ),
  //       cardEnabled: true,
  //     );
  //     BraintreeDropInResult result =
  //         await BraintreeDropIn.start(request);
  //     if (result != null) {
  //       showNonce(result.paymentMethodNonce);
  //     }
  //
  // }

  // payNow() async {
  //   BraintreePayment braintreePayment = new BraintreePayment();
  //   var data = await braintreePayment.showDropIn(
  //       nonce: clientNonce, amount: "2.0", enableGooglePay: true, nameRequired:true);
  //   print("Response of the payment $data");
  // }
  PropertyDetailsPageState(
      this.property
      );

  void _setLoading(bool value) {
    if (!mounted) return;
    setState(() => _loading = value);
  }
  Future<void> getUserId() async {
    PrefManager manager = await PrefManager.getInstance();
    String user_id = await manager.getUserId();
    setState(() {
      userId = user_id;
    });

  }
  @override
  void initState() {
    super.initState();
getUserId();
    propertyLikeListener = RxCommandListener(
      propertyManager.propertyLikeDislike,
      onIsBusy: () => _setLoading(true),
      onNotBusy: () => _setLoading(false),
      onError: (error) {
        print('${error.toString()}');
      },
      onValue: (response) {
        if (!mounted) return;
        setState(() {
          PropertyLikeResponse propertyLikeResponse = response;
          if (propertyLikeResponse.status == 200) {
            setState(() {
              property = propertyLikeResponse.data;
            });
            DialogHelper.showSuccessToast(propertyLikeResponse.message);
          } else {
            DialogHelper.showErrorToast(propertyLikeResponse.message);
          }
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          property.currency + property.price + " AUD",
          textAlign: TextAlign.left,
          style: TextStyle(
            fontSize: 16.0,
            color: Colors.white,
            fontWeight: FontWeight.w600,
            letterSpacing: 0.5,
            fontFamily: "OpenSans",
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  decoration: new BoxDecoration(color: Colors.white),
                  height: 200,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 200.0,
                        // child: Image.network(
                        //   propertiesImage,
                        //   fit: BoxFit.cover,
                        // ),
                        child: CachedNetworkImage(
                          imageUrl:
                          property.image,
                          height: 200,
                          fit: BoxFit.cover,
                          placeholder: (context, value) =>
                              Container(
                                child: CupertinoActivityIndicator(),
                              ),
                          errorWidget: (context, value, error) =>
                              Container(
                                child: Icon(Icons.broken_image),
                              ),
                        ),

                      ),

                      Positioned(
                        top: 10,
                        right:
                            10, //give the values according to your requirement
                        //child: Icon(Icons.share),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            CupertinoButton(
                              minSize: double.minPositive,
                              padding: EdgeInsets.zero,
                              child: Icon(Icons.share,
                                  color: Colors.blue[700], size: 20),
                              onPressed: () {},
                            ),
                            SizedBox(width: 8),
                            SizedBox(
                              height: 28,
                              width: 30,
                              child: IconButton(
                                iconSize: 20,
                                onPressed: () {
                                  setState(() {
                                    var req = PropertyLikeParameter(
                                        id: userId,
                                        propertyId: property.id,
                                        type: !(property.like)
                                    );
                                    propertyManager.propertyLikeDislike.execute(req);
                                  });
                                },
                                icon: !property.like
                                    ? Icon(Icons.favorite_border,
                                        color: Colors.blue[700], size: 20)
                                    : Icon(Icons.favorite,
                                        color: Colors.red[300], size: 20),
                              ),
                            ),

                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
              padding: new EdgeInsets.only(left: 8, right: 8),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                             property.currency + property.price + " AUD",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.blue[900],
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5,
                                fontFamily: "OpenSans",
                              ),
                            ),
                            Container(
                                height: 14,
                                child: VerticalDivider(
                                    thickness: 1, color: Colors.blue[900])),
                            Text(
                              property.name,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5,
                                fontFamily: "OpenSans",
                              ),
                            ),
                            SizedBox(width: 5),
                            Image.asset(
                              "images/shield.png",
                              color: Colors.greenAccent[400],
                              height: 15,
                              width: 15,
                            ),
                          ],
                        ),
                        SizedBox(height: 5),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CupertinoButton(
                                minSize: double.minPositive,
                                padding: EdgeInsets.zero,
                                child: Icon(Icons.location_on,
                                    color: Colors.black87, size: 14),
                                onPressed: () {},
                              ),
                              Text(
                                property.location,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 10.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  letterSpacing: 0.5,
                                  fontFamily: "OpenSans",
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Spacer(),
                    MaterialButton(
                      onPressed: () {
                        // Fluttertoast.showToast(
                        //     msg: propertyId,
                        //     toastLength: Toast.LENGTH_SHORT,
                        //     gravity: ToastGravity.CENTER,
                        //     timeInSecForIosWeb: 1,
                        //     backgroundColor: Colors.black,
                        //     textColor: Colors.white,
                        //     fontSize: 12.0);
                        //payNow();
                        //initBrainTreePayment(property.price);
                      },
                      color: Colors.blue[900],
                      minWidth: 0,
                      height: 24,
                      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),

                      //padding: EdgeInsets.all(0),
                      child: Text(
                        'Buy',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 10,
                            fontFamily: 'OpenSans',
                            fontWeight: FontWeight.w600),
                      ),
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(6.0),
                        side: BorderSide(
                            color: Colors.transparent,
                            width: 1,
                            style: BorderStyle.solid),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: new EdgeInsets.only(top: 3),
              height: 0.5,
              width: double.infinity,
              color: Colors.grey,
            ),
            Container(
              padding: new EdgeInsets.only(top: 5, left: 8, right: 8),
              child: Row(
                children: [
                  new Text(
                    'Covered Area',
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                  Spacer(),
                  new Text(
                    'Locality',
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: new EdgeInsets.only(top: 3, left: 8, right: 8),
              child: Row(
                children: [
                  new Text(
                    property.coveredArea,
                    style: TextStyle(
                      fontSize: 8.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                  Spacer(),
                  new Text(
                    property.location+ property.locality,
                    style: TextStyle(
                      fontSize: 8.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                ],
              ),
            ),
            // Container(
            //   padding: new EdgeInsets.only(top: 3, left: 8, right: 8),
            //   child: Row(
            //     children: [
            //       new Text(
            //         '\$ 4000 AUD/Sqft',
            //         style: TextStyle(
            //           fontSize: 8.0,
            //           color: Colors.black,
            //           fontWeight: FontWeight.w600,
            //           letterSpacing: 0.5,
            //           fontFamily: "OpenSans",
            //         ),
            //       ),
            //     ],
            //   ),
            // ),
            Container(
              padding: new EdgeInsets.only(top: 8, left: 8, right: 8),
              child: Row(
                children: [
                  new Text(
                    'Society',
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                  Spacer(),
                  new Text(
                    'Configuraton',
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: new EdgeInsets.only(top: 3, left: 8, right: 8),
              child: Row(
                children: [
                  new Text(
                    property.society,
                    style: TextStyle(
                      fontSize: 8.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                  Spacer(),
                  new Text(
                    property.configuration,
                    style: TextStyle(
                      fontSize: 8.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: new EdgeInsets.only(top: 8, left: 8, right: 8),
              child: Row(
                children: [
                  new Text(
                    'Possession',
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                  Spacer(),
                  new Text(
                    'Status',
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding:
                  new EdgeInsets.only(top: 3, left: 8, right: 8, bottom: 5),
              child: Row(
                children: [
                  new Text(
                    property.possesstion,
                    style: TextStyle(
                      fontSize: 8.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                  Spacer(),
                  new Text(
                    property.status,
                    style: TextStyle(
                      fontSize: 8.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: new EdgeInsets.only(top: 5),
              height: 0.5,
              width: double.infinity,
              color: Colors.grey,
            ),
            Container(
              padding: new EdgeInsets.only(top: 5, left: 8, right: 8),
              child: Row(
                children: [
                  Text(
                    'Description',
                    style: TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.5,
                      fontFamily: "OpenSans",
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(8, 4, 8, 8),
              child: Text(
                property.description,
                //textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 11.0,
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0.5,
                  fontFamily: "OpenSans",
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // ignore: missing_return
  Future<Messages> addToWishList(String propertiesid, String favStatus) async {
    final http.Response response = await http.post(
      'https://fituniverse.com/EWC/api/FavResponse',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'properties_id': propertiesid,
        'status_fav': favStatus,
      }),
    );
    if (response.statusCode == 200) {
      //pr.hide();
      var statusMsg = Messages.fromJson(jsonDecode(response.body));
      //SharedPreferences prefs = await SharedPreferences.getInstance();
      if (statusMsg.status == 1) {
        print(statusMsg.status);
        print(statusMsg.message);
// ignore: invalid_use_of_protected_member
        (context as Element).reassemble();
        // Fluttertoast.showToast(
        //     msg: statusMsg.message,
        //     toastLength: Toast.LENGTH_SHORT,
        //     gravity: ToastGravity.CENTER,
        //     timeInSecForIosWeb: 1,
        //     backgroundColor: Colors.black,
        //     textColor: Colors.white,
        //     fontSize: 12.0);

      } else {
        print(statusMsg.message);
        Fluttertoast.showToast(
            msg: statusMsg.message,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.black,
            textColor: Colors.white,
            fontSize: 12.0);
      }
      print(statusMsg.status);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to load Login');
    }
  }
}
