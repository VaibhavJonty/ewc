import 'dart:convert';

import 'package:EWC/controller/properties_helper.dart';
import 'package:EWC/drawerpage/appbaricon.dart';
import 'package:EWC/drawerpage/navigationDrawer.dart';
import 'package:EWC/fragment/favourite_property.dart';
import 'package:EWC/fragment/property_details.dart';
import 'package:EWC/models/index.dart';
import 'package:EWC/models/masterResponse.dart';
import 'package:EWC/models/messages.dart';
import 'package:EWC/models/property/PropertyListResponse.dart';
import 'package:EWC/models/propertyitem.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flappy_search_bar/search_bar_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:imagebutton/imagebutton.dart';
import 'package:http/http.dart' as http;
import 'package:EWC/packages.dart';
import 'filter_overlay.dart';

class propertyPage extends StatefulWidget {
  static const String routeName = '/propertyPage';

  @override
  _propertyPageState createState() => _propertyPageState();
}

List<Propertyitem> data;



class _propertyPageState extends State<propertyPage>
    with SingleTickerProviderStateMixin {
  PropertyManager propertyManager = Injector.get();
  RxCommandListener<PropertyParameter, PropertyListResponse>
      propertyListListener;
  RxCommandListener<PropertyLikeParameter, PropertyLikeResponse>
  propertyLikeListener;
  RxCommandListener<void, MasterResponse>
  masterDataListener;
  bool _loading = false;
  PropertyListResponse propertyListResponse;
  List<Property> propertyList;
  List<Property> searchablePropertyList;
  int page = 1;
  ScrollController _scrollController = new ScrollController();
  bool isLoading = false;
  bool isPropertyAvailable= true;
  int likePosition = 0;
  String userId;
  TextEditingController _textController = TextEditingController();
  MasterResponse masterResponse;
  void _setLoading(bool value) {
    if (!mounted) return;
    setState(() => _loading = value);
  }

  Future<void> getPropertList() async {
    PrefManager manager = await PrefManager.getInstance();
    String user_id = await manager.getUserId();
    setState(() {
      userId = user_id;
    });
    var req = PropertyParameter(
        id: userId,
        page: page,

        );
    propertyManager.propertyListing.execute(req);
  }

  Future<void> getMasterData() async {

    propertyManager.getMasterData.execute("");
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    propertyList = [];
    searchablePropertyList = [];
    getMasterData();
    getPropertList();
    masterDataListener = RxCommandListener(
      propertyManager.getMasterData,
      onIsBusy: () => _setLoading(true),
      onNotBusy: () => _setLoading(false),
      onError: (error) {
        print('${error.toString()}');
      },
      onValue: (response) {
        if (!mounted) return;
        setState(() {
          masterResponse = response;
          if (masterResponse.status == 200) {

            //DialogHelper.showSuccessToast(propertyListResponse.message);
          } else {
            DialogHelper.showErrorToast(propertyListResponse.message);
          }
        });
      },
    );
    propertyListListener = RxCommandListener(
      propertyManager.propertyListing,
      onIsBusy: () => _setLoading(true),
      onNotBusy: () => _setLoading(false),
      onError: (error) {
        print('${error.toString()}');
      },
      onValue: (response) {
        if (!mounted) return;
        setState(() {
          propertyListResponse = response;
          if (propertyListResponse.status == 200) {
            if(page==1){
              propertyList.clear();
            }
            if(propertyListResponse.data.length<10){
              isPropertyAvailable = false;
            }
            setState(() {
              propertyList.addAll(propertyListResponse.data);
              searchablePropertyList = List.from(propertyList);
            });
            //DialogHelper.showSuccessToast(orderListResponse.message);
          } else {
            DialogHelper.showErrorToast(propertyListResponse.message);
          }
        });
      },
    );

    propertyLikeListener = RxCommandListener(
      propertyManager.propertyLikeDislike,
      onIsBusy: () => _setLoading(true),
      onNotBusy: () => _setLoading(false),
      onError: (error) {
        print('${error.toString()}');
      },
      onValue: (response) {
        if (!mounted) return;
        setState(() {
          PropertyLikeResponse propertyLikeResponse = response;
          if (propertyLikeResponse.status == 200) {
            setState(() {
              propertyList[likePosition] = propertyLikeResponse.data;
              searchablePropertyList = List.from(propertyList);
            });
            DialogHelper.showSuccessToast(propertyLikeResponse.message);
          } else {
            DialogHelper.showErrorToast(propertyLikeResponse.message);
          }
        });
      },
    );
  }

  onItemChanged(String value) {
    setState(() {
      searchablePropertyList = propertyList
          .where((property) => property.name.toLowerCase().contains(value.toLowerCase()))
          .toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: navigationDrawer(),
        appBar: AppBar(
          elevation: 0.0,
          title: Text(
            "Property List",
            style: TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontFamily: 'OpenSans',
                fontWeight: FontWeight.w600),
          ),
          actions: [
            AppBarIconPage(),
          ],
        ),
        floatingActionButton: FloatingActionButton(onPressed: () async {
          if(masterResponse!=null){
            var data = await showDialog(
              context: context,
              builder: (_) => FilterOverlay(masterResponse),
            );
            if (data!=null){
              PropertyFilterParameter filterData = data;
              var req = PropertyParameter(
                id: userId,
                page: 1,
                start_price: filterData.startPrice,
                end_price: filterData.endPrice,
                cat_filter: filterData.category,
                location_filter: filterData.location

              );
              propertyManager.propertyListing.execute(req);
            }
          }

        },backgroundColor: Colors.greenAccent[400],
          child: Icon(Icons.filter_alt_outlined, color: Colors.white, size: 29,),
        ),

        body: ModalProgressHUD(
          inAsyncCall: _loading,
          progressIndicator: Loader(),
          child: Column(
            children: [
              // Container(
              //   padding: new EdgeInsets.only(left: 16.0, right: 16.0),
              //   child: Row(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     mainAxisAlignment: MainAxisAlignment.center,
              //     children: [
              //       MaterialButton(
              //         onPressed: () {},
              //         //color: Colors.blue,
              //         minWidth: 0,
              //         height: 28,
              //         padding: EdgeInsets.all(5),
              //         child: Text(
              //           'Restaurants',
              //           style: TextStyle(
              //               color: Colors.blue[900],
              //               fontSize: 14,
              //               fontFamily: 'OpenSans',
              //               fontWeight: FontWeight.w600),
              //         ),
              //         shape: new RoundedRectangleBorder(
              //           borderRadius: new BorderRadius.circular(10.0),
              //           side: BorderSide(
              //               color: Colors.grey,
              //               width: 1,
              //               style: BorderStyle.solid),
              //         ),
              //       ),
              //       //Spacer(),
              //       SizedBox(
              //         width: 4,
              //       ),
              //       MaterialButton(
              //         onPressed: () {},
              //         //color: Colors.blue,
              //         minWidth: 50,
              //         height: 28,
              //         padding: EdgeInsets.all(5),
              //         child: Text(
              //           'Bar',
              //           style: TextStyle(
              //               color: Colors.blue[900],
              //               fontSize: 14,
              //               fontFamily: 'OpenSans',
              //               fontWeight: FontWeight.w600),
              //         ),
              //         shape: new RoundedRectangleBorder(
              //           borderRadius: new BorderRadius.circular(10.0),
              //           side: BorderSide(
              //               color: Colors.grey,
              //               width: 1,
              //               style: BorderStyle.solid),
              //         ),
              //       ),
              //       SizedBox(
              //         width: 4,
              //       ),
              //
              //       //Spacer(),
              //       MaterialButton(
              //         onPressed: () {},
              //         //color: Colors.blue,
              //         minWidth: 0,
              //         height: 28,
              //         padding: EdgeInsets.all(5),
              //         child: Text(
              //           'Night Clubs',
              //           style: TextStyle(
              //               color: Colors.blue[900],
              //               fontSize: 14,
              //               fontFamily: 'OpenSans',
              //               fontWeight: FontWeight.w600),
              //         ),
              //         shape: new RoundedRectangleBorder(
              //           borderRadius: new BorderRadius.circular(10.0),
              //           side: BorderSide(
              //               color: Colors.grey,
              //               width: 1,
              //               style: BorderStyle.solid),
              //         ),
              //       ),
              //       Spacer(),
              //       MaterialButton(
              //         onPressed: () async {
              //           var data = await showDialog(
              //             context: context,
              //             builder: (_) => FilterOverlay(),
              //           );
              //
              //         },
              //         color: Colors.greenAccent[400],
              //         minWidth: 0,
              //         height: 28,
              //         padding: EdgeInsets.all(5),
              //         child: Text(
              //           'Filters',
              //           style: TextStyle(
              //               color: Colors.white,
              //               fontSize: 14,
              //               fontFamily: 'OpenSans',
              //               fontWeight: FontWeight.w600),
              //         ),
              //         shape: new RoundedRectangleBorder(
              //           borderRadius: new BorderRadius.circular(10.0),
              //           side: BorderSide(
              //               color: Colors.greenAccent[400],
              //               width: 1,
              //               style: BorderStyle.solid),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),

              Padding(
                padding: new EdgeInsets.only(
                    left: 10.0, right: 10.0, top: 5, bottom: 5),
                child: Container(
                  height: 40,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: TextFormField(
                      style: new TextStyle(color: Colors.black),
                      textAlign: TextAlign.left,
                      maxLines: 1,
                      controller: _textController,
                      onChanged: onItemChanged,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.grey[200],
                        contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        suffixIcon: Container(
                          // transform: Matrix4.translationValues(-10.0, 0.0, 0.0),
                          padding:
                              const EdgeInsetsDirectional.only(start: 12.0),
                          child: Icon(
                            Icons.search,
                            color: Colors.blue[900],
                            size: 30.0,
                          ), // icon is 48px widget.
                        ),
                        // prefixIcon: Icon(
                        //   Icons.search,
                        //   color: Colors.grey,
                        //   size: 30.0,
                        // ),
                        border: new OutlineInputBorder(
                          borderSide: new BorderSide(color: Colors.grey[200]),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(10.0),
                          ),
                        ),
                        hintStyle:
                            TextStyle(color: Colors.grey, fontSize: 16.0),
                        hintText: 'Search',
                      ),
                    ),
                  ),
                ),
              ),
              // Padding(
              //   padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              //   child: Align(
              //     alignment: Alignment.centerLeft,
              //     child: new Text(data.length.toString()+" results found",

              //       textAlign: TextAlign.left,
              //       style: TextStyle(
              //         fontSize: 12.0,
              //         color: Colors.black,
              //         fontWeight: FontWeight.w600,
              //         letterSpacing: 0.5,
              //         fontFamily: "OpenSans",
              //       ),
              //     ),
              //   ),
              // ),
              NotificationListener<ScrollNotification>(
                // ignore: missing_return
                  onNotification: (ScrollNotification scrollInfo) {
                    print("scrollInfo.metrics.pixels " +
                        scrollInfo.metrics.pixels.toString() +
                        "------" +
                        scrollInfo.metrics.maxScrollExtent.toString());

                    if (!isLoading &&
                        scrollInfo.metrics.pixels ==
                            scrollInfo.metrics.maxScrollExtent && isPropertyAvailable) {
                      page++;
                      setState(() {
                        isLoading = true;
                      });
                      getPropertList();
                    }
                  },
                  child: Expanded(
                    child: searchablePropertyList.isNotEmpty ?  _listProperty(searchablePropertyList) : Container(),
                  ))

            ],
          ),
        ));
  }


  ListView _listProperty(List<Property> data) {
    return ListView.builder(
      itemCount: data.length,
      physics:  AlwaysScrollableScrollPhysics (),
      itemBuilder: (BuildContext context, int index) =>
          buildPropertyCard(context, data[index], index),
    );
  }

  buildPropertyCard(BuildContext context, Property property, int position) {
    return Card(
      margin: EdgeInsets.all(6),
      elevation: 3.0,
      color: Colors.indigo[50],
      child: Padding(
        padding: EdgeInsets.all(0.0),

        //padding: EdgeInsets.only(left: 10.0, right: 10.0),
        child: Material(
          //elevation: 1.0,
          color: Colors.transparent,

          borderRadius: BorderRadius.circular(10.0),
          child: new GestureDetector(
            onTap: () {
              //print("Container clicked");

              Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (BuildContext context) => new PropertyDetailsPage(
                      property),
                ),
              );

              // Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //       builder: (context) => PropertyDetailsPage()),
              // );
            },
            child: Container(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Material(
                          elevation: 0.2,
                          // borderRadius: BorderRadius.all(Radius.elliptical(50.0, 50.0)),
                          child: Container(
                            width: 100.0,
                            height: 100.0,
                            child: FadeInImage.assetNetwork(
                              placeholder: 'images/app_logo.jpg',
                              image: property.image,
                              height: 200,
                              fit: BoxFit.cover,
                              fadeInDuration: new Duration(milliseconds: 100),
                            ),
                            // CachedNetworkImage(
                            //   imageUrl:
                            //   property.image,
                            //   height: 200,
                            //   fit: BoxFit.cover,
                            //   placeholder: (context, value) =>
                            //       Container(
                            //         child: CupertinoActivityIndicator(),
                            //       ),
                            //   errorWidget: (context, value, error) =>
                            //       Container(
                            //         child: Icon(Icons.broken_image),
                            //       ),
                            // ),

                          ),
                        ),
                      ),
                      Container(
                        //padding: new EdgeInsets.only(left: 8.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            new Text(
                              "\$" + property.price + " AUD",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 16.0,
                                color: Colors.blue[900],
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5,
                                fontFamily: "OpenSans",
                              ),
                            ),
                            new Text(
                              property.location,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5,
                                fontFamily: "OpenSans",
                              ),
                            ),
                            SizedBox(height: 5),
                            new Text(
                              property.name,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5,
                                fontFamily: "OpenSans",
                              ),
                            ),
                            new Text(
                              property.coveredArea,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5,
                                fontFamily: "OpenSans",
                              ),
                            ),
                            new Text(
                              property.ownerType,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.5,
                                fontFamily: "OpenSans",
                              ),
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Container(
                        //color: Colors.pink,
                        height: 120.0,
                        width: 50,
                        child: Stack(
                          children: [
                            Positioned(
                              // top: 5,
                              right: 5,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  SizedBox(
                                    height: 27,
                                    width: 30,
                                    child: IconButton(
                                      iconSize: 15,
                                      onPressed: () {
                                        setState(() {
                                          likePosition = position;
                                          var req = PropertyLikeParameter(
                                              id: userId,
                                              propertyId: property.id,
                                              type: !(property.like)
                                          );
                                          propertyManager.propertyLikeDislike.execute(req);
                                        });
                                      },
                                      icon: !property.like
                                          ? Icon(Icons.favorite_border,
                                              color: Colors.blue[900], size: 20)
                                          : Icon(Icons.favorite,
                                              color: Colors.red[300], size: 20),
                                    ),
                                  ),

                                  SizedBox(width: 5),
                                  CupertinoButton(
                                    minSize: double.minPositive,
                                    padding: EdgeInsets.zero,
                                    child: Image.asset(
                                      "images/shield.png",
                                      color: Colors.greenAccent[400],
                                      height: 18,
                                      width: 16,
                                    ),
                                    onPressed: () {},
                                  ),
                                  // SizedBox(
                                  //   height: 30,
                                  //   //width: 30,
                                  //   child:
                                  // ),
                                ],
                              ),
                            ),
                            Positioned(
                              bottom: -13,
                              right: 0,
                              child: MaterialButton(
                                onPressed: () {},
                                color: Colors.blue[900],
                                minWidth: 50,
                                height: 20,
                                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),

                                //padding: EdgeInsets.all(0),
                                child: Text(
                                  'Buy',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 10,
                                      fontFamily: 'OpenSans',
                                      fontWeight: FontWeight.w600),
                                ),
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.only(
                                      topLeft: Radius.circular(8.0),
                                      bottomRight: Radius.circular(8.0)),
                                  side: BorderSide(
                                      color: Colors.transparent,
                                      width: 1,
                                      style: BorderStyle.solid),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

}

