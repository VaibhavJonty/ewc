import 'package:EWC/models/masterResponse.dart';
import 'package:EWC/models/property/PropertyListResponse.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

class FilterOverlay extends StatefulWidget {
 final MasterResponse masterResponse;
  @override
  State<StatefulWidget> createState() => FilterOverlayState();

 FilterOverlay(this.masterResponse);
}

class FilterOverlayState extends State<FilterOverlay>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  MasterResponse masterResponse;
  static String startValue,endValue;
  RangeValues values;
  RangeLabels labels;
  var _items;
  List<Location> locationList = [];
  List<Category> _selectedCategory = [];
  Location location;
  @override
  void initState() {
    super.initState();
    masterResponse = widget.masterResponse;
    startValue = masterResponse.data.priceRange.start;
    endValue = masterResponse.data.priceRange.end;
    values = RangeValues(double.parse(startValue), double.parse(endValue));
    labels = RangeLabels(startValue, endValue);
    _items = masterResponse.data.category
        .map((category) => MultiSelectItem<Category>(category, category.categoriesName))
        .toList();
    locationList = masterResponse.data.location;
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }


  int _value = 1;

  

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Container(
            margin: EdgeInsets.all(20.0),
            padding: EdgeInsets.all(5.0),
            width: size.width * 0.8,
            height: size.height * 0.6,
            decoration: ShapeDecoration(
                color: Colors.white,
                //color: Color.fromRGBO(41, 167, 77, 10),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0))),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  padding: new EdgeInsets.only(top: 5, left: 8, right: 8),
                  child: Row(
                    children: [
                      Align(
                        alignment: Alignment.topLeft,
                        child: new Text(
                          "Price",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14.0,
                              fontFamily: "OpenSans",
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  //padding: new EdgeInsets.only(left: 8, right: 8),
                  child: new RangeSlider(
                    min: double.parse(startValue),
                    max: double.parse(endValue),
                    values: values,
                    divisions: 20,
                    activeColor: Colors.greenAccent[400],
                    labels: labels,
                    onChanged: (value) {
                      print('START: ${value.start}, END: ${value.end}');
                      setState(() {
                        values = value;

                        labels = RangeLabels(
                            '${value.start.toInt().toString()}\$',
                            '${value.end.toInt().toString()}\$');
                      });
                    },
                  ),
                ),
                Align(
                    alignment: Alignment.topLeft,

                    //padding: new EdgeInsets.only(left: 8, right: 8),
                    child: Padding(
                      padding: new EdgeInsets.only(left: 8, right: 8),
                      child: Row(
                        children: [
                          new Text(
                            '\$' + startValue,
                            style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.greenAccent[400],
                              fontWeight: FontWeight.w600,
                              letterSpacing: 0.5,
                              fontFamily: "OpenSans",
                            ),
                          ),
                          Spacer(),
                          new Text(
                            '\$' + endValue,
                            style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.greenAccent[400],
                              fontWeight: FontWeight.w600,
                              letterSpacing: 0.5,
                              fontFamily: "OpenSans",
                            ),
                          ),
                        ],
                      ),
                    )),
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 5.0, left: 10.0, right: 5.0),
                    child: Text(
                      "Categories",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 14.0,
                          fontFamily: "OpenSans",
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
                MultiSelectChipField(
                  items: _items,
                  initialValue: [],
                  showHeader: false,
                  title: Text(
                    "",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 1.0,
                        fontFamily: "OpenSans",
                        fontWeight: FontWeight.w600),
                  ),
                  headerColor: Colors.white,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white, width: 1.8),
                  ),
                  selectedChipColor: Colors.blue[900].withOpacity(0.5),
                  selectedTextStyle: TextStyle(color: Colors.blue[900]),
                  onTap: (values) {
                    var list = values;
                    _selectedCategory = list.cast<Category>();
                  },

                ),

                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 5.0, left: 10.0, right: 5.0),
                    child: Text(
                      "Location",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 14.0,
                          fontFamily: "OpenSans",
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  //padding: EdgeInsets.all(4.0),
                  child: Padding(
                      padding: const EdgeInsets.only(
                          top: 10.0, left: 10.0, right: 10.0),
                      child: Container(
                         padding: const EdgeInsets.only(
                          top: 0.0, left: 10.0, right: 10.0),
                        height: 30,
                        width: size.width * 0.7,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          // border: Border(
                          //   left: BorderSide(
                          //     color: Colors.green,
                          //     width: 3,
                          //   ),
                          // ),

                          color: Colors.white,
                          border: Border.all( color: Colors.grey[300],),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<Location>(
                            value: location,
                            icon: Image.asset(
                              "images/down_arrow.png",
                              color: Colors.greenAccent[400],
                              height: 16,
                              width: 16,
                            ),
                            iconSize: 10,
                            elevation: 16,
                            style: TextStyle(
                                color: Colors.blue[900],
                                fontSize: 14.0,
                                fontFamily: "OpenSans",
                                fontWeight: FontWeight.w600),
                            underline: Container(height: 2, color: Colors.grey),
                            onChanged: (Location newValue) {
                              setState(() {
                                location = newValue;
                              });
                            },
                            items: locationList.map<DropdownMenuItem<Location>>((Location value) {
                              return DropdownMenuItem<Location>(
                                value: value,
                                child: Text(value.name),
                              );
                            }).toList(),
                          ),
                        ),
                      )

                      ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 10.0, left: 10.0, right: 5.0,bottom: 10),
                    child: MaterialButton(
                      onPressed: () {
                        String selectedCategory="";
                        String locationId = "";
                        if(location!=null){
                          locationId = location.name;
                        }
                        for(int i=0; i<_selectedCategory.length; i++){
                          selectedCategory = selectedCategory + _selectedCategory[i].categoriesUniqueId+",";
                        }
                        if (selectedCategory.endsWith(",")) {
                          selectedCategory = selectedCategory.substring(0, selectedCategory.length-2);
                        }
                        var data = PropertyFilterParameter(category: selectedCategory,startPrice: values.start.toInt().toString(),endPrice: values.end.toInt().toString(),
                        location: locationId);
                        Navigator.pop(context,data);
                      },
                      color: Colors.greenAccent[400],
                      minWidth: 0,
                      height: 24,
                      padding: EdgeInsets.fromLTRB(5, 4, 5, 4),

                      //padding: EdgeInsets.all(0),
                      child: Text(
                        'SUBMIT',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13,
                            fontFamily: 'OpenSans',
                            fontWeight: FontWeight.w600),
                      ),
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(6.0),
                        side: BorderSide(
                            color: Colors.transparent,
                            width: 1,
                            style: BorderStyle.solid),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
