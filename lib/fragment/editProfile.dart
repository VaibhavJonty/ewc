import 'package:EWC/drawerpage/navigationDrawer.dart';
import 'package:EWC/managers/auth_manager.dart';
import 'package:EWC/models/auth/edit_profile_response.dart';
import 'package:flutter/material.dart';

import '../injector.dart';
import '../packages.dart';



class EditProfile extends StatefulWidget {
  static const String routeName = '/EditProfile';
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> with SingleTickerProviderStateMixin{
  AuthManager authManager = Injector.get();
  RxCommandListener<EditProfileParameter, EditProfileResponse> editProfileListener;
  bool _loading = false;
  EditProfileResponse loginResponse;
  EditProfileParameter editProfileParameter;
  String userId;
  String name,email,mobile;
  void _setLoading(bool value) {
    if (!mounted) return;
    setState(() => _loading = value);
  }

  final nameController = new TextEditingController();
  final mobileController = new TextEditingController();
  final emailController = new TextEditingController();

  final FocusNode nameFocus = FocusNode();
  final FocusNode mobileFocus = FocusNode();
  final FocusNode emailFocus = FocusNode();

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setData();
    editProfileListener = RxCommandListener(
      authManager.editProfile,
      onIsBusy: () => _setLoading(true),
      onNotBusy: () => _setLoading(false),
      onError: (error) {
        print('${error.toString()}');
      },
      onValue: (response) {
        if (!mounted) return;
        setState(() {
          loginResponse = response;
          if (loginResponse.status == "1") {
            saveUser();
            DialogHelper.showSuccessToast(loginResponse.message);
          } else {
            DialogHelper.showSuccessDialog(context, loginResponse.message);
          }
        });
      },
    );
  }

  Future<void> saveUser() async {
    // PrefManager manager = await PrefManager.getInstance();
    // manager.setFirstName(editProfileParameter.first_name);
    // manager.setLastName(editProfileParameter.last_name);
    Navigator.pop(context);
  }
  @override
  void dispose() {
    editProfileListener.dispose();
    super.dispose();
  }

  Future<void> setData() async {
    //PrefManager manager = await PrefManager.getInstance();
    SharedPreferences prefs = await SharedPreferences.getInstance();
     userId = prefs.getString('user_id');
    name = prefs.getString('user_name');
    email = prefs.getString('user_email');
    mobile = prefs.getString('user_mobile');

    nameController.text = name;
    emailController.text = email;

    //userNameController.text = user.username;
  }

  bool isValid() {
    String name, lastName, displayName, email;
    name = nameController.text;
    email = emailController.text;
    mobile = mobileController.text;
    editProfileParameter = new EditProfileParameter(
        id: userId,
        name: name,
        mobile: mobile,
      );

    if (name.isEmpty) {
      DialogHelper.showErrorToast("Please enter name");
      return false;
    }


    if (mobile.isEmpty) {
      DialogHelper.showErrorToast("Please enter mobile no");
      return false;
    }
    if (mobile.length<10) {
      DialogHelper.showErrorToast("Please enter valid mobile no");
      return false;
    } else {
      return true;
    }
  }

  void editProfile(EditProfileParameter editProfileParameter) {
    KeyboardHelper.hideKeyboard(context);
    authManager.editProfile.execute(editProfileParameter);
  }

  Widget nameText(TextEditingController controller, String hintText,
      FocusNode currentFocus, FocusNode nextFocus) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.1 / 1.5,
        width: MediaQuery.of(context).size.width / 1.2,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            color: Colors.white.withOpacity(0.16),
            borderRadius: BorderRadius.all(Radius.circular(1))),
        child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
                padding: const EdgeInsets.only(left: 20),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  controller: controller,
                  focusNode: currentFocus,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    // contentPadding: EdgeInsets.all(8),
                    enabled: true,
                    // labelText: "Username",
                    // labelStyle: TextStyle(
                    //   color: emailFocus.hasFocus ? Colors.cyan : Colors.grey,
                    //   fontWeight: FontWeight.w300,
                    // ),
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                    alignLabelWithHint: true,
                    hintText: hintText,
                    hintStyle: TextStyle(color: Colors.black.withOpacity(0.5)),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                  ),
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(context, currentFocus, nextFocus);
                  },
                ))));
  }
  Widget mobileText(TextEditingController controller, String hintText,
      FocusNode currentFocus, FocusNode nextFocus) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.1 / 1.5,
        width: MediaQuery.of(context).size.width / 1.2,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            color: Colors.white.withOpacity(0.16),
            borderRadius: BorderRadius.all(Radius.circular(1))),
        child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
                padding: const EdgeInsets.only(left: 20),
                child: TextFormField(
                  keyboardType: TextInputType.phone,
                  controller: controller,
                  focusNode: currentFocus,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    // contentPadding: EdgeInsets.all(8),
                    enabled: true,
                    // labelText: "Username",
                    // labelStyle: TextStyle(
                    //   color: emailFocus.hasFocus ? Colors.cyan : Colors.grey,
                    //   fontWeight: FontWeight.w300,
                    // ),
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                    alignLabelWithHint: true,
                    hintText: hintText,
                    hintStyle: TextStyle(color: Colors.black.withOpacity(0.5)),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                  ),
                  onFieldSubmitted: (term) {
                    FocusScope.of(context).unfocus();
                    //_fieldFocusChange(context, currentFocus, nextFocus);
                  },
                ))));
  }
  Widget emailText(TextEditingController controller, String hintText,
      FocusNode currentFocus, FocusNode nextFocus) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.1 / 1.5,
        width: MediaQuery.of(context).size.width / 1.2,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            color: Colors.white.withOpacity(0.16),
            borderRadius: BorderRadius.all(Radius.circular(1))),
        child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
                padding: const EdgeInsets.only(left: 20),
                child: TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  controller: controller,
                  focusNode: currentFocus,

                  style: TextStyle(
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    // contentPadding: EdgeInsets.all(8),
                    enabled: false,
                    // labelText: "Username",
                    // labelStyle: TextStyle(
                    //   color: emailFocus.hasFocus ? Colors.cyan : Colors.grey,
                    //   fontWeight: FontWeight.w300,
                    // ),
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                    alignLabelWithHint: true,
                    hintText: hintText,
                    hintStyle: TextStyle(color: Colors.black.withOpacity(0.5)),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                  ),
                  // onFieldSubmitted: (term) {
                  //   _fieldFocusChange(context, currentFocus, nextFocus);
                  // },
                ))));
  }
  Widget showText(String value) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.1 / 2,
        width: MediaQuery.of(context).size.width / 1.2,
        decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.16),
            borderRadius: BorderRadius.all(Radius.circular(1))),
        child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              value,
              style: TextStyle(fontSize: 20, color: Colors.blue[900]),
            )));
  }

  Widget spaceValue(height, value) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * height / value,
    );
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: navigationDrawer(),
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text('Edit Profile'),
      ),
      body: ModalProgressHUD(
        inAsyncCall: _loading,
        progressIndicator: Loader(),
        child: SingleChildScrollView(
          child: Container(
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height,
            ),
            width: double.maxFinite,
            child: Column(
              children: [
                spaceValue(0.1, 5),
                showText('Name'),
                nameText(
                    nameController, 'Name', nameFocus, mobileFocus),
                spaceValue(0.1, 5),
                showText('Mobile'),
                mobileText(mobileController, 'Mobile', mobileFocus,
                    emailFocus),
                spaceValue(0.1, 5),
                showText("E-mail"),
                emailText(
                    emailController, 'Email', mobileFocus, mobileFocus),
                spaceValue(0.1, 2),
              ],
            ),
          ),
        ),
      ),
      persistentFooterButtons: [
        SizedBox(
          height: 60,
          width: double.maxFinite,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 16.0,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: RawMaterialButton(
                    constraints: BoxConstraints(
                      minHeight: 50,
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    onPressed: ()  {
                      if (isValid()) {
                        editProfile(editProfileParameter);
                      }
                    },
                    fillColor: Colors.blue[900],
                    child: Text(
                      "Save",
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 17,
                          color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
