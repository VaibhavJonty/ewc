
import '../packages.dart';

abstract class StringHelper {
  static String boolToYesNo(bool value) {
    if (value == true) return 'Yes';
    return 'No';
  }

  static bool yesNoToBool(String value) {
    if (value != null && value.toLowerCase() == 'yes') return true;
    return false;
  }

  static int boolToZeroOne(bool value) {
    if (value == true) return 1;
    return 0;
  }

  static bool zeroOneToBool(int value) {
    if (value == 1) return true;
    return false;
  }
  ///
  /// Capitalize first letter of the string
  /// return "" if given string is null or empty
  /// else return new string with first letter capital.
  /// Example:
  /// "t" => T
  /// "toy" => Toy
  /// null => ""
  /// "" => ""
  ///
  static String capitalizeFirstLetter(String s) {
    if (s == null || s.isEmpty) return "";
    return s[0].toUpperCase() + s.substring(1);
  }

  static bool isNullOrBlank(String s) {
    return s == null || s.trim().isEmpty;
  }

  static bool isNotNullOrBlank(String s) {
    return s != null && s.trim().isNotEmpty;
  }

  ///
  /// Get message string from exception. When:
  ///
  /// [null] return [MessageConstants.errUnknownErrorOccurred]
  ///
  /// [PlatformException] return exception's message
  ///
  /// Else return [Object.toString()]
  ///
  static String getMessage(dynamic exception) {
    if (exception == null) return StringConstants.errUnknownErrorOccurred;
    return (exception is PlatformException)
        ? exception.message
        : exception.toString();
  }
}
