
import '../packages.dart';

abstract class DateHelper {
  static DateTime parseServerDateTime(String str) {
    if (str == null || str.isEmpty) return null;
    return DateConstants.serverDateFormat.parse(str);
  }

  static DateTime parseDeapartureDateTime(String str) {
    if (str == null || str.isEmpty) return null;
    return DateConstants.yearMonthDayFormat.parse(str);
  }
}
