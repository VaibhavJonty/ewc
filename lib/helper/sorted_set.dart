import 'dart:collection';

class SortedSet<T> {
  SortedSet(this._comparator)
      : _items = SplayTreeSet((v1, v2) => v1 == v2 ? 0 : _comparator(v1, v2));

  final Set<T> _items;
  final Comparator<T> _comparator;

  int get length => _items.length;

  add(T value) {
    _items.remove(value);
    _items.add(value);
  }

  addAll(List<T> values) {
    _items.removeAll(values);
    _items.addAll(values);
  }

  remove(T value) {
    _items.remove(value);
  }

  clear() {
    _items.clear();
  }

  T get(int index) {
    return _items.elementAt(index);
  }

  T first({T defaultValue}) {
    if (_items.isEmpty)
      return defaultValue;
    else
      return _items.first;
  }

  T last({T defaultValue}) {
    if (_items.isEmpty)
      return defaultValue;
    else
      return _items.last;
  }
}
