import 'package:flutter/material.dart';

abstract class RouteHelper {
  ///
  /// Navigate to next screen
  /// [rootNavigator] pass true to navigation on root context
  /// @return [Future] result of push method
  ///
  static Future<T> navigate<T extends Object>(
    BuildContext context,
    Widget widget, {
    bool rootNavigator = false,
    bool popSelf = false,
  }) {
    if (popSelf) {
      return Navigator.of(context, rootNavigator: rootNavigator)
          .pushReplacement(MaterialPageRoute(builder: (_) => widget));
    }
    return Navigator.of(context, rootNavigator: rootNavigator).push(
      MaterialPageRoute(builder: (_) => widget),
    );
  }
}
