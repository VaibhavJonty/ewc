import 'package:flutter/services.dart';

import '../packages.dart';

abstract class AppHelper {
  /// Update system UI
  static void setSystemUI() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: ColorConstants.whiteDark,
      statusBarBrightness: Brightness.dark,
    ));
  }
}
