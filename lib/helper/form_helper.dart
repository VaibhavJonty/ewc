abstract class FormHelper {
  static const errRequiredField = 'Required field';

  static String notEmptyValidator(String value) {
    if (value == null || value.trim().isEmpty) {
      return errRequiredField;
    }
    return null;
  }

  static String usernameValidator(String value,String username) {
    if (value == null || value.trim().isEmpty || value==username) {
      return 'Username already taken';
    }
    return null;
  }

  static String productValidator(String value) {
    if (value == 'Select your product') {
      return 'Please select product';
    }
    return null;
  }
  static String complainValidator(String value) {
    if (value == 'Complain type') {
      return 'Please select complain type';
    }
    return null;
  }
}
