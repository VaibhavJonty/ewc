import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../packages.dart';

class DialogHelper {
  static final border = RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(DimenConstants.dialogCornerRadius),
  );

  static Future showDialogWithOneButton(
    BuildContext context,
    String title,
    String content, {
    String buttonLabel = StringConstants.okButton,
    VoidCallback onButtonPress,
    barrierDismissible = true,
  }) {
    return showDialog(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext buildContext) {
        return AlertDialog(
          title: Text(title, textAlign: TextAlign.center),
          content: Text(content),
          shape: border,
          actions: <Widget>[
            FlatButton(
              child: Text(buttonLabel),
              onPressed: () {
                if (onButtonPress != null) {
                  onButtonPress();
                } else {
                  Navigator.of(context, rootNavigator: true).pop();
                }
              },
            )
          ],
        );
      },
    );
  }

  static Future showDialogWithTwoButtons(
    BuildContext context,
    String title,
    String content, {
    String positiveButtonLabel = StringConstants.okButton,
    VoidCallback positiveButtonPress,
    String negativeButtonLabel = StringConstants.cancelButton,
    VoidCallback negativeButtonPress,
    barrierDismissible = true,
  }) {
    return showDialog(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext buildContext) {
        return AlertDialog(
          title: Text(title, textAlign: TextAlign.center),
          content: Text(content),
          shape: border,
          actions: <Widget>[
            FlatButton(
              child: Text(negativeButtonLabel),
              textColor: Colors.black87,
              onPressed: () {
                if (negativeButtonPress != null) {
                  negativeButtonPress();
                } else {
                  Navigator.of(context, rootNavigator: true).pop();
                }
              },
            ),
            FlatButton(
              child: Text(positiveButtonLabel),
              onPressed: () {
                if (positiveButtonPress != null) {
                  positiveButtonPress();
                } else {
                  Navigator.of(context, rootNavigator: true).pop();
                }
              },
            )
          ],
        );
      },
    );
  }

  static Future showToast(String message) {
    return Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_LONG,
    );
  }

  static Future showSuccessToast(String message) {
    return Fluttertoast.showToast(
      msg: message,
      backgroundColor: Colors.green,
      textColor: Colors.white,
      toastLength: Toast.LENGTH_SHORT,
    );
  }

  static Future showSuccessDialog(BuildContext context, String message) {
    return DialogHelper.showDialogWithOneButton(
      context,
      StringConstants.popupTitleSuccess,
      message,
    );
  }

  static Future showErrorToast(dynamic error) {
    return Fluttertoast.showToast(
      msg: StringHelper.getMessage(error),
      backgroundColor: Colors.red,
      textColor: Colors.white,
      toastLength: Toast.LENGTH_SHORT,
    );
  }

  static Future showErrorDialog(BuildContext context, String error) {
    return DialogHelper.showDialogWithOneButton(
      context,
      StringConstants.popupTitleError,
      StringHelper.getMessage(error),
    );
  }
}
