
import '../packages.dart';

abstract class ApiHelper {
  static String getImageThumbnailUrl(String image) {
    if (image == null) return ImageConstants.appLogo;
    return ApiConstants.baseImageUrl + '/' + image;
  }

  static String getImageFullUrl(String image) {
    if (image == null) return ImageConstants.appLogo;
    return ApiConstants.baseImageUrl + '/' + image;
  }
}
