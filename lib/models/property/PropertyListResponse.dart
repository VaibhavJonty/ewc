import 'package:flutter/widgets.dart';

class PropertyListResponse {
  int _status;
  String _message;
  List<Property> _data;

  PropertyListResponse({int status, String message, List<Property> data}) {
    this._status = status;
    this._message = message;
    this._data = data;
  }

  int get status => _status;
  set status(int status) => _status = status;
  String get message => _message;
  set message(String message) => _message = message;
  List<Property> get data => _data;
  set data(List<Property> data) => _data = data;

  PropertyListResponse.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _message = json['message'];
    if (json['data'] != null) {
      _data = new List<Property>();
      json['data'].forEach((v) {
        _data.add(new Property.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['message'] = this._message;
    if (this._data != null) {
      data['data'] = this._data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Property {
  String _id;
  String _categoryId;
  String _subCategoryId;
  String _name;
  String _price;
  String _currency;
  String _location;
  String _coveredArea;
  String _society;
  String _possesstion;
  String _locality;
  String _configuration;
  String _status;
  String _description;
  String _lattitude;
  String _longitude;
  String _image;
  String _flag;
  String _ownerType;
  String _ownerName;
  String _mobile;
  bool _like;

  Property(
      {String id,
        String categoryId,
        String subCategoryId,
        String name,
        String price,
        String currency,
        String location,
        String coveredArea,
        String society,
        String possesstion,
        String locality,
        String configuration,
        String status,
        String description,
        String lattitude,
        String longitude,
        String image,
        String flag,
        String ownerType,
        String ownerName,
        String mobile,
        bool like}) {
    this._id = id;
    this._categoryId = categoryId;
    this._subCategoryId = subCategoryId;
    this._name = name;
    this._price = price;
    this._currency = currency;
    this._location = location;
    this._coveredArea = coveredArea;
    this._society = society;
    this._possesstion = possesstion;
    this._locality = locality;
    this._configuration = configuration;
    this._status = status;
    this._description = description;
    this._lattitude = lattitude;
    this._longitude = longitude;
    this._image = image;
    this._flag = flag;
    this._ownerType = ownerType;
    this._ownerName = ownerName;
    this._mobile = mobile;
    this._like = like;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get categoryId => _categoryId;
  set categoryId(String categoryId) => _categoryId = categoryId;
  String get subCategoryId => _subCategoryId;
  set subCategoryId(String subCategoryId) => _subCategoryId = subCategoryId;
  String get name => _name;
  set name(String name) => _name = name;
  String get price => _price;
  set price(String price) => _price = price;
  String get currency => _currency;
  set currency(String currency) => _currency = currency;
  String get location => _location;
  set location(String location) => _location = location;
  String get coveredArea => _coveredArea;
  set coveredArea(String coveredArea) => _coveredArea = coveredArea;
  String get society => _society;
  set society(String society) => _society = society;
  String get possesstion => _possesstion;
  set possesstion(String possesstion) => _possesstion = possesstion;
  String get locality => _locality;
  set locality(String locality) => _locality = locality;
  String get configuration => _configuration;
  set configuration(String configuration) => _configuration = configuration;
  String get status => _status;
  set status(String status) => _status = status;
  String get description => _description;
  set description(String description) => _description = description;
  String get lattitude => _lattitude;
  set lattitude(String lattitude) => _lattitude = lattitude;
  String get longitude => _longitude;
  set longitude(String longitude) => _longitude = longitude;
  String get image => _image;
  set image(String image) => _image = image;
  String get flag => _flag;
  set flag(String flag) => _flag = flag;
  String get ownerType => _ownerType;
  set ownerType(String ownerType) => _ownerType = ownerType;
  String get ownerName => _ownerName;
  set ownerName(String ownerName) => _ownerName = ownerName;
  String get mobile => _mobile;
  set mobile(String mobile) => _mobile = mobile;
  bool get like => _like;
  set like(bool like) => _like = like;

  Property.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _categoryId = json['category_id'];
    _subCategoryId = json['sub_category_id'];
    _name = json['name'];
    _price = json['price'];
    _currency = json['currency'];
    _location = json['location'];
    _coveredArea = json['covered_area'];
    _society = json['society'];
    _possesstion = json['possesstion'];
    _locality = json['locality'];
    _configuration = json['configuration'];
    _status = json['status'];
    _description = json['description'];
    _lattitude = json['lattitude'];
    _longitude = json['longitude'];
    _image = json['image'];
    _flag = json['flag'];
    _ownerType = json['owner_type'];
    _ownerName = json['owner_name'];
    _mobile = json['mobile'];
    _like = json['like'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['category_id'] = this._categoryId;
    data['sub_category_id'] = this._subCategoryId;
    data['name'] = this._name;
    data['price'] = this._price;
    data['currency'] = this._currency;
    data['location'] = this._location;
    data['covered_area'] = this._coveredArea;
    data['society'] = this._society;
    data['possesstion'] = this._possesstion;
    data['locality'] = this._locality;
    data['configuration'] = this._configuration;
    data['status'] = this._status;
    data['description'] = this._description;
    data['lattitude'] = this._lattitude;
    data['longitude'] = this._longitude;
    data['image'] = this._image;
    data['flag'] = this._flag;
    data['owner_type'] = this._ownerType;
    data['owner_name'] = this._ownerName;
    data['mobile'] = this._mobile;
    data['like'] = this._like;
    return data;
  }
}

class PropertyParameter {
  final int page;
  final String id;
  final String cat_filter;
  final String location_filter;
  final String start_price;
  final String end_price;

  PropertyParameter({
    @required this.page,
    @required this.id,
    @required this.cat_filter,
    @required this.location_filter,
    @required this.start_price,
    @required this.end_price,
  });
}

class PropertyLikeParameter {
  final String propertyId;
  final String id;
  final bool type;


  PropertyLikeParameter({
    @required this.propertyId,
    @required this.id,
    @required this.type,
  });
}

class PropertyDetailParameter {
  final String propertyId;
  final String id;



  PropertyDetailParameter({
    @required this.propertyId,
    @required this.id,
  });
}

class PropertyLikeResponse {
  int _status;
  String _message;
  Property _data;


  PropertyLikeResponse({int status, String message, Property data}) {
    this._status = status;
    this._message = message;
    this._data = data;
  }

  int get status => _status;
  set status(int status) => _status = status;
  String get message => _message;
  set message(String message) => _message = message;
  Property get data => _data;
  set data(Property data) => _data = data;

  PropertyLikeResponse.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? new Property.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['message'] = this._message;
    if (this._data != null) {
      data['data'] = this._data.toJson();
    }
    return data;
  }
}

class PropertyDetailResponse {
  int _status;
  String _message;
  Property _data;

  PropertyDetailResponse({int status, String message, Property data}) {
    this._status = status;
    this._message = message;
    this._data = data;
  }

  int get status => _status;
  set status(int status) => _status = status;
  String get message => _message;
  set message(String message) => _message = message;
  Property get data => _data;
  set data(Property data) => _data = data;

  PropertyDetailResponse.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? new Property.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['message'] = this._message;
    if (this._data != null) {
      data['data'] = this._data.toJson();
    }
    return data;
  }


}

class PropertyFilterParameter {
  final String startPrice;
  final String endPrice;
  final String category;
  final String location;


  PropertyFilterParameter({
    @required this.startPrice,
    @required this.endPrice,
    @required this.category,
    @required this.location,
  });
}