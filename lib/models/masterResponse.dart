class MasterResponse {
  int _status;
  String _message;
  Data _data;

  MasterResponse({int status, String message, Data data}) {
    this._status = status;
    this._message = message;
    this._data = data;
  }

  int get status => _status;
  set status(int status) => _status = status;
  String get message => _message;
  set message(String message) => _message = message;
  Data get data => _data;
  set data(Data data) => _data = data;

  MasterResponse.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['message'] = this._message;
    if (this._data != null) {
      data['data'] = this._data.toJson();
    }
    return data;
  }
}

class Data {
  List<Category> _category;
  List<Location> _location;
  PriceRange _priceRange;

  Data(
      {List<Category> category,
        List<Location> location,
        PriceRange priceRange}) {
    this._category = category;
    this._location = location;
    this._priceRange = priceRange;
  }

  List<Category> get category => _category;
  set category(List<Category> category) => _category = category;
  List<Location> get location => _location;
  set location(List<Location> location) => _location = location;
  PriceRange get priceRange => _priceRange;
  set priceRange(PriceRange priceRange) => _priceRange = priceRange;

  Data.fromJson(Map<String, dynamic> json) {
    if (json['category'] != null) {
      _category = new List<Category>();
      json['category'].forEach((v) {
        _category.add(new Category.fromJson(v));
      });
    }
    if (json['location'] != null) {
      _location = new List<Location>();
      json['location'].forEach((v) {
        _location.add(new Location.fromJson(v));
      });
    }
    _priceRange = json['price_range'] != null
        ? new PriceRange.fromJson(json['price_range'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._category != null) {
      data['category'] = this._category.map((v) => v.toJson()).toList();
    }
    if (this._location != null) {
      data['location'] = this._location.map((v) => v.toJson()).toList();
    }
    if (this._priceRange != null) {
      data['price_range'] = this._priceRange.toJson();
    }
    return data;
  }
}

class Category {
  String _categoriesUniqueId;
  String _categoriesId;
  String _categoriesName;
  String _insertedIp;
  String _insertedBy;
  String _insertedDate;
  String _updatedIp;
  String _updatedBy;
  String _updatedDate;
  String _activeFlag;
  String _delFlag;
  String _flag;

  Category(
      {String categoriesUniqueId,
        String categoriesId,
        String categoriesName,
        String insertedIp,
        String insertedBy,
        String insertedDate,
        String updatedIp,
        String updatedBy,
        String updatedDate,
        String activeFlag,
        String delFlag,
        String flag}) {
    this._categoriesUniqueId = categoriesUniqueId;
    this._categoriesId = categoriesId;
    this._categoriesName = categoriesName;
    this._insertedIp = insertedIp;
    this._insertedBy = insertedBy;
    this._insertedDate = insertedDate;
    this._updatedIp = updatedIp;
    this._updatedBy = updatedBy;
    this._updatedDate = updatedDate;
    this._activeFlag = activeFlag;
    this._delFlag = delFlag;
    this._flag = flag;
  }

  String get categoriesUniqueId => _categoriesUniqueId;
  set categoriesUniqueId(String categoriesUniqueId) =>
      _categoriesUniqueId = categoriesUniqueId;
  String get categoriesId => _categoriesId;
  set categoriesId(String categoriesId) => _categoriesId = categoriesId;
  String get categoriesName => _categoriesName;
  set categoriesName(String categoriesName) => _categoriesName = categoriesName;
  String get insertedIp => _insertedIp;
  set insertedIp(String insertedIp) => _insertedIp = insertedIp;
  String get insertedBy => _insertedBy;
  set insertedBy(String insertedBy) => _insertedBy = insertedBy;
  String get insertedDate => _insertedDate;
  set insertedDate(String insertedDate) => _insertedDate = insertedDate;
  String get updatedIp => _updatedIp;
  set updatedIp(String updatedIp) => _updatedIp = updatedIp;
  String get updatedBy => _updatedBy;
  set updatedBy(String updatedBy) => _updatedBy = updatedBy;
  String get updatedDate => _updatedDate;
  set updatedDate(String updatedDate) => _updatedDate = updatedDate;
  String get activeFlag => _activeFlag;
  set activeFlag(String activeFlag) => _activeFlag = activeFlag;
  String get delFlag => _delFlag;
  set delFlag(String delFlag) => _delFlag = delFlag;
  String get flag => _flag;
  set flag(String flag) => _flag = flag;

  Category.fromJson(Map<String, dynamic> json) {
    _categoriesUniqueId = json['categories_unique_id'];
    _categoriesId = json['categories_id'];
    _categoriesName = json['categories_name'];
    _insertedIp = json['inserted_ip'];
    _insertedBy = json['inserted_by'];
    _insertedDate = json['inserted_date'];
    _updatedIp = json['updated_ip'];
    _updatedBy = json['updated_by'];
    _updatedDate = json['updated_date'];
    _activeFlag = json['active_flag'];
    _delFlag = json['del_flag'];
    _flag = json['flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['categories_unique_id'] = this._categoriesUniqueId;
    data['categories_id'] = this._categoriesId;
    data['categories_name'] = this._categoriesName;
    data['inserted_ip'] = this._insertedIp;
    data['inserted_by'] = this._insertedBy;
    data['inserted_date'] = this._insertedDate;
    data['updated_ip'] = this._updatedIp;
    data['updated_by'] = this._updatedBy;
    data['updated_date'] = this._updatedDate;
    data['active_flag'] = this._activeFlag;
    data['del_flag'] = this._delFlag;
    data['flag'] = this._flag;
    return data;
  }
}

class Location {
  String _id;
  String _name;
  String _flag;

  Location({String id, String name, String flag}) {
    this._id = id;
    this._name = name;
    this._flag = flag;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get flag => _flag;
  set flag(String flag) => _flag = flag;

  Location.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _flag = json['flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['flag'] = this._flag;
    return data;
  }
}

class PriceRange {
  String _id;
  String _start;
  String _end;
  String _currency;
  String _flag;

  PriceRange(
      {String id, String start, String end, String currency, String flag}) {
    this._id = id;
    this._start = start;
    this._end = end;
    this._currency = currency;
    this._flag = flag;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get start => _start;
  set start(String start) => _start = start;
  String get end => _end;
  set end(String end) => _end = end;
  String get currency => _currency;
  set currency(String currency) => _currency = currency;
  String get flag => _flag;
  set flag(String flag) => _flag = flag;

  PriceRange.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _start = json['start'];
    _end = json['end'];
    _currency = json['currency'];
    _flag = json['flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['start'] = this._start;
    data['end'] = this._end;
    data['currency'] = this._currency;
    data['flag'] = this._flag;
    return data;
  }
}
