// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'logindata.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Logindata _$LogindataFromJson(Map<String, dynamic> json) {
  return Logindata(
      id: json['id'] as String,
      name: json['name'] as String,
      email: json['email'] as String,
      mobile: json['mobile'] as String
     
      );
  
}

Map<String, dynamic> _$LogindataToJson(Logindata instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'email': instance.email,
      'mobile': instance.mobile
    };
