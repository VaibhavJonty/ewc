import 'package:flutter/material.dart';

class ForgotPasswordResponse {
  int _status;
  String _message;
  OtpData _otpData;

  ForgotPasswordResponse({int status, String message, OtpData otpData}) {
    this._status = status;
    this._message = message;
    this._otpData = otpData;
  }

  int get status => _status;
  set status(int status) => _status = status;
  String get message => _message;
  set message(String message) => _message = message;
  OtpData get otpData => _otpData;
  set otpData(OtpData otpData) => _otpData = otpData;

  ForgotPasswordResponse.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _message = json['message'];
    _otpData =
    json['OtpData'] != null ? new OtpData.fromJson(json['OtpData']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['message'] = this._message;
    if (this._otpData != null) {
      data['OtpData'] = this._otpData.toJson();
    }
    return data;
  }
}

class OtpData {
  String _email;
  int _otp;

  OtpData({String email, int otp}) {
    this._email = email;
    this._otp = otp;
  }

  String get email => _email;
  set email(String email) => _email = email;
  int get otp => _otp;
  set otp(int otp) => _otp = otp;

  OtpData.fromJson(Map<String, dynamic> json) {
    _email = json['email'];
    _otp = json['otp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this._email;
    data['otp'] = this._otp;
    return data;
  }
}

class ChangePasswordParameter {
  final String email;
  final String otp;
  final String password;


  ChangePasswordParameter({
    @required this.email,
    @required this.otp,
    @required this.password

  });
}
