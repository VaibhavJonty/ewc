import 'package:flutter/cupertino.dart';

class EditProfileResponse {
  int status;
  String message;
  LoginData loginData;

  EditProfileResponse({this.status, this.message, this.loginData});

  EditProfileResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    loginData = json['LoginData'] != null
        ? new LoginData.fromJson(json['LoginData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.loginData != null) {
      data['LoginData'] = this.loginData.toJson();
    }
    return data;
  }
}

class LoginData {
  String id;
  String uniqid;
  String deviceid;
  String email;
  String mobile;
  String name;
  String password;
  String flag;
  String createDate;
  String cdate;
  String updateDate;
  String verifyOtp;

  LoginData(
      {this.id,
        this.uniqid,
        this.deviceid,
        this.email,
        this.mobile,
        this.name,
        this.password,
        this.flag,
        this.createDate,
        this.cdate,
        this.updateDate,
        this.verifyOtp});

  LoginData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    uniqid = json['uniqid'];
    deviceid = json['deviceid'];
    email = json['email'];
    mobile = json['mobile'];
    name = json['name'];
    password = json['password'];
    flag = json['flag'];
    createDate = json['create_date'];
    cdate = json['cdate'];
    updateDate = json['update_date'];
    verifyOtp = json['verify_otp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uniqid'] = this.uniqid;
    data['deviceid'] = this.deviceid;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['name'] = this.name;
    data['password'] = this.password;
    data['flag'] = this.flag;
    data['create_date'] = this.createDate;
    data['cdate'] = this.cdate;
    data['update_date'] = this.updateDate;
    data['verify_otp'] = this.verifyOtp;
    return data;
  }
}

class EditProfileParameter {
  final String id;
  final String mobile;
  final String name;


  EditProfileParameter({
    @required this.id,
    @required this.mobile,
    @required this.name

  });
}