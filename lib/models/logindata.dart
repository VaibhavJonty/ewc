import 'package:json_annotation/json_annotation.dart';

part 'logindata.g.dart';

@JsonSerializable()
class Logindata {
  String id;
  String name;
  String email;
  String mobile;

  Logindata({this.id, this.name, this.email, this.mobile});

  factory Logindata.fromJson(Map<String, dynamic> json) =>
      _$LogindataFromJson(json);
  Map<String, dynamic> toJson() => _$LogindataToJson(this);
}
