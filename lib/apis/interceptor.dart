
import '../packages.dart';

class AuthInterceptor implements InterceptorsWrapper {
  @override
  Future onRequest(RequestOptions options) async {
//    options.headers.addAll({
//      HttpHeaders.authorizationHeader: await ApiConstants.loadAuthorizationHeader(),
//    });
    return options;
  }

  @override
  Future onResponse(Response response) async {
    return response;
  }

  @override
  Future onError(DioError err) async {
    return err;
  }
}
