import 'package:EWC/models/auth/edit_profile_response.dart';
import 'package:EWC/models/auth/forgot_password.dart';
import 'package:EWC/models/masterResponse.dart';
import 'package:EWC/models/property/PropertyListResponse.dart';

import '../packages.dart';
import '../packages.dart';

abstract class WebApi {
 // Future<RegisterResponse> registerWithEmail(String email);

  Future<PropertyListResponse> getPropertyList(PropertyParameter propertyParameter);
  Future<ForgotPasswordResponse> forgotPassword(String email);
  Future<ForgotPasswordResponse> changePassword(ChangePasswordParameter changePasswordParameter);
  Future<PropertyDetailResponse> getPropertyDetail(PropertyDetailParameter propertyDetailParameter);
  Future<PropertyLikeResponse> getPropertyLike(PropertyLikeParameter propertyLikeParameter);
  Future<PropertyListResponse> getLikedProperty(String id);
  Future<MasterResponse> getMasterData(String id);
  Future<EditProfileResponse> editProfile(EditProfileParameter editProfileParameter);
}

class BaseWebApi extends WebApi {
  var pageLimit = "1";

  BaseWebApi(this.dio);

  final Dio dio;

  // @override
  // Future<RegisterResponse> registerWithEmail(String email) async {
  //   try {
  //     Response response = await dio.post(
  //       ApiConstants.register,
  //       data: {
  //         'email': email,
  //       },
  //     );
  //     Map map = response.data;
  //     return RegisterResponse.fromJson(map);
  //   } catch (error, stack) {
  //     print('error=$error, stack=$stack');
  //     rethrow;
  //   }
  // }


  @override
  Future<PropertyListResponse> getPropertyList(PropertyParameter propertyParameter) async {
    try {
      // var parameters = {"page": propertyParameter.page, "id": propertyParameter.id};
      // if (propertyParameter.start_price.isNotEmpty){
      //   parameters.addEntries({"start_price": propertyParameter.start_price})
      // }
      var params = {"page": propertyParameter.page, "id": propertyParameter.id,"cat_filter":propertyParameter.cat_filter,
        "location_filter":propertyParameter.location_filter,"start_price":propertyParameter.start_price,"end_price":propertyParameter.end_price};
      print(params);
      Response response = await Dio().get(
        ApiConstants.baseUrl+ApiConstants.propertyListing,
        queryParameters: params,
      );

      Map map = response.data;
      print(response.data);
      return PropertyListResponse.fromJson(map);
    } catch (error, stack) {
      print('error=$error, stack=$stack');
      rethrow;
    }
  }

  @override
  Future<ForgotPasswordResponse> forgotPassword(String email) async {
    try {
      Response response = await Dio().get(
        ApiConstants.baseUrl+ApiConstants.forgotPassword,
        queryParameters: {"email": email},
      );

      Map map = response.data;
      return ForgotPasswordResponse.fromJson(map);
    } catch (error, stack) {
      print('error=$error, stack=$stack');
      rethrow;
    }
  }

  @override
  Future<ForgotPasswordResponse> changePassword(ChangePasswordParameter changePasswordParameter) async {
    try {
      Response response = await Dio().get(
        ApiConstants.baseUrl+ApiConstants.changePassword,
        queryParameters:
        {
          "email": changePasswordParameter.email,
          "otp": changePasswordParameter.otp,
          "password" :changePasswordParameter.password
        },
      );

      Map map = response.data;
      return ForgotPasswordResponse.fromJson(map);
    } catch (error, stack) {
      print('error=$error, stack=$stack');
      rethrow;
    }
  }

  @override
  Future<PropertyDetailResponse> getPropertyDetail(PropertyDetailParameter propertyDetailParameter) async {
    try {
      Response response = await Dio().get(
        ApiConstants.baseUrl+ApiConstants.propertyDetails,
        queryParameters: {"property_id": propertyDetailParameter.propertyId, "id": propertyDetailParameter.id},
      );

      Map map = response.data;
      return PropertyDetailResponse.fromJson(map);
    } catch (error, stack) {
      print('error=$error, stack=$stack');
      rethrow;
    }
  }

  @override
  Future<PropertyLikeResponse> getPropertyLike(PropertyLikeParameter propertyLikeParameter) async {
    try {
      Response response = await Dio().get(
        ApiConstants.baseUrl+ApiConstants.propertyLike,
        queryParameters:
        {
          "property_id": propertyLikeParameter.propertyId,
          "id": propertyLikeParameter.id,
          "type" :propertyLikeParameter.type
        },
      );

      Map map = response.data;
      print(response.data);
      return PropertyLikeResponse.fromJson(map);
    } catch (error, stack) {
      print('error=$error, stack=$stack');
      rethrow;
    }
  }

  @override
  Future<PropertyListResponse> getLikedProperty(String userId) async {
    try {
      Response response = await Dio().get(
        ApiConstants.baseUrl+ApiConstants.favPropertyListing,
        queryParameters: {"id": userId},
      );

      Map map = response.data;
      return PropertyListResponse.fromJson(map);
    } catch (error, stack) {
      print('error=$error, stack=$stack');
      rethrow;
    }
  }

  @override
  Future<MasterResponse> getMasterData(String id) async {
    try {
      Response response = await Dio().get(
        ApiConstants.baseUrl+ApiConstants.searchBarListing,
      );

      Map map = response.data;
      return MasterResponse.fromJson(map);
    } catch (error, stack) {
      print('error=$error, stack=$stack');
      rethrow;
    }
  }

  @override
  Future<EditProfileResponse> editProfile(EditProfileParameter editProfileParameter) async {
    try {
      Response response = await dio.post(
        ApiConstants.editProfile,
        data: {
          'name': editProfileParameter.name,
          'mobile': editProfileParameter.mobile,
          'id': editProfileParameter.id,
        },
      );
      Map map = response.data;
      return EditProfileResponse.fromJson(map);
    } catch (error, stack) {
      print('error=$error, stack=$stack');
      rethrow;
    }
  }
}
