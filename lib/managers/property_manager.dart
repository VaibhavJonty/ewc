import 'package:EWC/models/masterResponse.dart';
import 'package:EWC/models/property/PropertyListResponse.dart';

import '../packages.dart';

abstract class PropertyManager {
  RxCommand<PropertyDetailParameter, PropertyDetailResponse> propertyDetail;
  RxCommand<PropertyParameter, PropertyListResponse> propertyListing;
  RxCommand<PropertyLikeParameter, PropertyLikeResponse> propertyLikeDislike;
  RxCommand<String, PropertyListResponse> likedPropertyListing;
  RxCommand<String, MasterResponse> getMasterData;
}

class BasePropertyManager extends PropertyManager {
  final WebApi _api;

  BasePropertyManager(this._api) {
    propertyDetail = RxCommand.createAsync(_api.getPropertyDetail, emitLastResult: false);
    propertyListing = RxCommand.createAsync(_api.getPropertyList, emitLastResult: false);
    propertyLikeDislike = RxCommand.createAsync(_api.getPropertyLike, emitLastResult: false);
    likedPropertyListing = RxCommand.createAsync(_api.getLikedProperty, emitLastResult: false);
    getMasterData = RxCommand.createAsync(_api.getMasterData, emitLastResult: false);
  }
}
