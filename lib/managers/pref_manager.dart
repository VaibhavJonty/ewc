
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import 'dart:ui';

class PrefManager extends ChangeNotifier{
  static const userId = 'userId';
  static const user = 'user';
  static const isLogIn = 'isLoggedIn';
  static const token = 'token';
  static const cartCount = 'cartCount';
  static PrefManager _instance;
  static SharedPreferences prefs;
  static const staticData = 'staticData';
  static const stateData = 'stateData';
  static const firstname = 'firstname';
  static const lastname = 'lastname';

  addStringToSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('stringValue', "abc");
  }

  static Future<PrefManager> getInstance() async {
    if (_instance == null) {
      _instance = PrefManager();
    }
    if (prefs == null) {
      prefs = await SharedPreferences.getInstance();
    }
    return _instance;
  }

  void setIsLoggedIn(bool isLoggedIn) async {
    prefs.setBool(PrefManager.isLogIn, isLoggedIn);
  }

  Future<bool> isLoggedIn() async {
    return prefs.getBool(PrefManager.isLogIn);
  }

  void setUserId(String userId) async {
    prefs.setString(PrefManager.userId, userId);
  }

  Future<String> getUserId() async {
    return prefs.getString(PrefManager.userId);
  }

  void setCartCount(int cartCount) async {
    prefs.setInt(PrefManager.cartCount, cartCount);
  }

  Future<int> getCartCount() async {
    return prefs.getInt(PrefManager.cartCount);
  }

  void setToken(String token) async {
    prefs.setString(PrefManager.token, token);
  }

  String getToken()  {
    return prefs.get(PrefManager.token);
  }

  void setFirstTimeLaunch(bool isFirstTime) async {
    prefs.setBool('IS_FIRST_TIME_LAUNCH', isFirstTime);
  }

  Future<bool> isFirstTimeLaunch() async {
    prefs = await SharedPreferences.getInstance();
    return prefs.getBool('IS_FIRST_TIME_LAUNCH') ?? false;
  }

  addIntToSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('intValue', 123);
  }

  addDoubleToSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setDouble('doubleValue', 115.0);
  }

  addBoolToSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('boolValue', true);
  }

  getValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    String stringValue = prefs.getString('stringValue');
    //Return bool
    bool boolValue = prefs.getBool('boolValue');
    //Return int
    //int intValue = prefs.getInt('intValue');

    int intValue = prefs.getInt('intValue') ?? 0;
    //Return double
    double doubleValue = prefs.getDouble('doubleValue');
  }

  removeValues() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Remove String
    prefs.remove("stringValue");
    //Remove bool
    prefs.remove("boolValue");
    //Remove int
    prefs.remove("intValue");
    //Remove double
    prefs.remove("doubleValue");
  }

  //Check value if present or not?
  checkValues() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    bool CheckValue = prefs.containsKey('value');
  }

  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  readObjects(String key) async {
    final prefs = await SharedPreferences.getInstance();
    String value = prefs.getString(key);
    if (value == null) {
      return null;
    } else {
      print(prefs.getString(key));
      return json.decode(prefs.getString(key));
    }
  }

  saveObject(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    String user = json.encode(value);
    prefs.setString(key, user);
  }

  removeObject(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  clearPref() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  void setFirstName(String firstname) async {
    prefs.setString(PrefManager.firstname, firstname);
  }

  String getFirstName()  {
    return prefs.get(PrefManager.firstname);
  }

  void setLastName(String lastname) async {
    prefs.setString(PrefManager.lastname, lastname);
  }

  String getLastName()  {
    return prefs.get(PrefManager.lastname);
  }
}
