import 'package:EWC/models/auth/edit_profile_response.dart';
import 'package:EWC/models/auth/forgot_password.dart';
import 'package:EWC/models/property/PropertyListResponse.dart';

import '../packages.dart';

abstract class AuthManager {
  RxCommand<String, ForgotPasswordResponse> forgotPassword;
  RxCommand<ChangePasswordParameter, ForgotPasswordResponse> changePassword;
  RxCommand<EditProfileParameter, EditProfileResponse> editProfile;
}

class BaseAuthManager extends AuthManager {
  final WebApi _api;

  BaseAuthManager(this._api) {
    forgotPassword = RxCommand.createAsync(_api.forgotPassword, emitLastResult: false);
    changePassword = RxCommand.createAsync(_api.changePassword, emitLastResult: false);
    editProfile = RxCommand.createAsync(_api.editProfile, emitLastResult: false);
  }
}
